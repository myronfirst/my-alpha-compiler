# Compiler for Alpha: A dynamically typed language

## TODOS
- Change Memory management system, see section below
- Remove forward references and reduce the amount of files

## Dependencies
- Install [Bazel](https://bazel.build/) build system for building and testing the project
- Install [Bazelisk](https://github.com/bazelbuild/bazelisk?tab=readme-ov-file) to fetch the latest version of bazel
- Install [Buildifier](https://github.com/bazelbuild/buildtools/blob/master/buildifier/README.md) for linting BUILD and WORKSPACE files (not required)

## Alpha Language
This language compiler consists of lexical and syntax analysis, intermediate and target code generation, and lastly, execution of the language in a VM.

Alpha is a dynamically typed language with syntax similar to javascript.
Notably, Alpha supports function objects and dictionaries of variable type keys and values.

The compiler accepts a single Alpha language file.
The `.alpha` file extension is preferable.

The vocabulary and syntax of the language is described in the project description of the Language and Compilers course [webpage](https://www.csd.uoc.gr/~hy340/).

The vocabulary and syntax have a lot of similarities to the Javascript language.

## Using the Compiler
To compile your Alpha language file pass it as a parameter to the compiler executable.

The compilation output is printed to `stdout` unless a text file is passed as an optional second parameter.
```
./bin/AlphaCompiler source.alpha output.txt`
```

## Building the Compiler
To build the compiler from source use the [GNU make](https://www.gnu.org/software/make/manual/make.html) tool.
```
make all    //builds everything
make all -j //build everything using parallel jobs
make clean  //cleans intermediate and binary files
```

## Notes on Design Decisions

### Lexical Analysis
The Flex lexical scanner generator is used for lexical analysis.

String literals containing tab `'\t'` and newline `'\n'` characters (multi-line strings) are supported.

Supported escape character sequences are  `'\a'  '\b'  '\f' '\n' '\r' '\t' '\v' '\\' '\"'`.

When a non ascii character is detected, a warning message is printed.

### Syntax Analysis
The Bison grammar parser generator is used for syntax analysis.
A C++ LALR(1) parser is generated.

The most notable C++ parser features of bison is the support for of variant-based semantic values

`%nterm <std::string> non_terminal`

and the support for complete terminal symbols

`yy::parser::make_NUMBER(yytext);`

These features render the usage of the `%union` directive and of the `yylval` global variable obsolete.

The [dangling-else](https://en.wikipedia.org/wiki/Dangling_else) ambiguity manifested as shift/reduced conflict is resolved by specifying additional precedence rules for the `else` token.

In case of syntax error, the input is ignored until the next `;` token

A stack of named scopes is used to keep track of the active scope and its category.

Slightly modified scope rules are used compared to the ones in the project description.

In addition to scope rules regarding blocks and function arguments, we treat symbol declarations in `if`, `while` and `for` parentheses as part of the enclosing scope.
```
if(local a = 1)
    true;
if (local b = 1) {
    true;
}
while (local c = true)
    true;
for (local i = 0; i < 10; ++i)
    true;
//a, b, c, i are part their respective individual scopes (scope 1)
```

#### Semantic Analysis
For the semantic analysis of the language a Symbol Table is used.
The Symbol Table is useful for retrieving symbols during intermediate code generation.

The information stored for each symbol includes it's name, scope and definition location in the source file.

Two different designs for the Symbol Table are implemented
#### Stack of Symbol Maps
A `Symbol Map`, is used to map each symbol name to it's entry.
A `Symbol Map` is a Hash-Table implementation of a key-value store.

For scope resolution, a stack of `Symbol Maps` is used associating a `Symbol Map` to it's scope.
Upon scope end, the active `Symbol Map` is disabled and shadowed by a new empty one.

Symbol lookup takes *O(1)*, since we perform a Hash-Table lookup.

#### Symbol Set with ScopeLists
A `Symbol Set` is used to store each individual symbol entry.
A `Symbol Set` is a Hash-Table implementation of a set.

For scope resolution, a `Scope Lists` structure is used, associating a symbol to it's scope.
A `Scope Lists` is a stack of lists to symbol entry pointers.
Upon symbol insertion, the `Scope Lists` is updated.

Symbol lookup takes *O(n)*, since we linearly traverse each list in the `Scope Lists`.

### Intermediate Code Generation

#### Top-Level Design
- Lexical analysis internals to ScannerContext class, used on scanner rules
- Parsing internals to ParserContextClass, used on parser rules
- Compiler class receives input file, performs parsing and outputs to separate files, lexical tokens, symbol table and intermediate code

#### Memory Management
TODO: Implement the below. Switch from shared pointers
- An expression is allocated during parsing. A non-owning pointer to the expression is passed along the bison rules syntax tree, until it is passed to the QuadHolder holder class which retains ownership of the allocated memory.
- Expressions which are discarded during parsing before being stored on the QuadHolder, are manually deallocated.

#### Location Tracking
- Each terminal symbol comes with its associated location in the source. A symbol location is described by its row and column coordinates.
- We use our own location type for location tracking, which is based on the default location type generated by bison.
- On the Scanner side, a Location cursor is used to keep track of each terminal location. A symbol is constructed with its location and then returned to the parser.
- On the Parser side, most of the rules have a location parameter. This location encapsulates the source locations associated with the quad emissions of this rule, or a future along the parse tree.
- Constructing and passing the correct location parameters on each rule, is done sufficiently well. However, it has not been sufficiently tested in fine granularities.

#### QuadHolder and OpCodes
- Emitted instructions are stored in a QuadHolder type container.
- QuadHolder provides the emit methods.
- The UnaryMinus opcode is removed, since a UnaryMinus instruction is replaced by a multiplication by -1

#### Temporary Variables and Recycling
- Temporary Variables are reset at the end of a statement.
- Further actions have been taken to recycle temporary variables among quad emissions. Check Lectures.
- Variable recycling on table construction rule is not implemented, since we deem this case invalid.

#### Scope Spaces
- ScopeSpaces have been implemented using a stack, similarly to how Scope tracking has been implemented.
- Each element of the ScopeSpaceStack contains the current scope space name along with its counter.

#### Function Definitions
- We insert the function symbol to the symbol table before the function arguments and function body are parsed.
- This allows allows for calling a function whose function body is not yet completely defined.
- The function symbol also stores a link to its argument list

### Expression class
- Contains the expression type, a pointer to the associated symbol and a variant field.
- The pointer to it's associated symbol is null in cases where the variant field is valid. The pointer to symbol can also be null on some cases related to short circuit evaluation.
- The variant field is used to store constant types, evaluated during compile time, a pointer to the index of a table element, or backpatching label lists related to short circuit evaluation.

#### Assignments
- Usage of emit_iftable_element is eliminated. Cases where a table access or store emission is required, are handled individually per rule.
- If the result of an assignment is known at compile time, then we return the associated constant evaluated expression, without emitting an extra assign quad.
- We emit the extra assignment when the expression is not constant evaluated

#### Basic Expressions
- Different function call rules delegate their logic to a single block of code, responsible for emitting function call quads.
- `call : lvalue call_suffix` rule code has been expanded to eliminate redundant calls to emit_iftable_element

#### arithmetic comparative logical expressions
- We check the validity of constant evaluated operand types and notify the user on invalid use.
- On constant evaluated expressions we calculate the result and delegate it to an upper rule without emitting any quads

#### Branches, Loops
- Symbols inside the if/loop statement body have their own scope, even in cases of single statements without the use of brackets

#### Short Circuit Evaluation
- `expression: expression_not_eval` has been create to accommodate most of the extra emission cases. When an `expression_not_eval` is Short Circuit Evaluated and converted to an `expression`.
- The left operand and assignment / boolean operation is Short Circuit Evaluated before the second operand is parsed. Correct order of emits is guaranteed this way.

#### Testing
Intermediate code validation is performed by supplying the compiler with a source code input and comparing the intermediate code output with an expected value.

- The googletest framework is used for testing
- We supply a collection of input files along with expected output files. If an output file produced by an input source matches the corresponding output file, then the test passes.
- We do not account for location outputs in our testing.
- For inputs where an error is expected, we supply the test with a list strings expected to appear on the compiler error message. We capture the output and search it for the contents of the list. If all string elements exist, then the test passes

## Acknowledgements
This project is a re-work of a previous student project.

The original project was developed in 2018 for the Languages and Compilers course, Computer Science Department, University of Crete.

It was a collaborative student work among [Myron Tsatsarakis](https://gitlab.com/myronfirst) and [Zinovia Stefanidi](https://dblp.org/pid/221/9313.html).
