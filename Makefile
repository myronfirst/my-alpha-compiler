BIN				:=	bin
INTER			:=	intermediate
SRC				:=	src
SRC_GRAMMAR		:=	flex_bison
PCH				:=	pch
TARGET			:=	AlphaCompiler
OBJECTS			:=	$(TARGET).o Parser.o Scanner.o ScannerHandlers.o ParserHandlers.o SymbolTable.o Symbol.o Expression.o QuadHolder.o

LEX				:= flex
LFLAGS			:= --debug
YACC			:= bison
YFLAGS			:= --debug -Wall --color --report=all -Wcounterexamples -Wdangling-alias #-Wno-counterexamples -Wno-precedence
CXX     		:= g++-12
CXXFLAGS		:= -g -O0 -std=c++20 -iquote $(PCH) -iquote $(INTER) -iquote $(SRC) -D_GLIBCXX_DEBUG \
-Wall -Wextra -pedantic -pedantic-errors  \
-Wcast-align -Wcast-qual -Wconversion \
-Wfloat-equal -Winit-self \
-Wmissing-field-initializers \
-Wmissing-include-dirs -Wmissing-noreturn \
-Wpacked -Wpointer-arith \
-Wredundant-decls \
-Wshadow -Wstack-protector \
-Wswitch-default \
-Wunreachable-code -Wunused -Wunused-parameter\
-Wwrite-strings -Winvalid-pch\
#-O2 -DNDEBUG
VFLAGS := -s --leak-check=full --show-leak-kinds=all --track-origins=yes --log-file=valgrind-out.txt


.PHONY: parallel all clean memcheck
.DEFAULT_GOAL := all

parallel:
	$(info Invoke Jobs)
	@$(MAKE) -j all

all: $(BIN)/$(TARGET)

$(BIN)/$(TARGET): $(OBJECTS:%.o=$(INTER)/%.o)
	$(info Linking $@)
	@$(CXX) $(CXXFLAGS) -o $@ $^

$(INTER)/$(TARGET).o: $(SRC)/$(TARGET).cpp $(SRC)/SymbolTable.cpp $(INTER)/Parser.cpp $(INTER)/Scanner.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
$(INTER)/QuadHolder.o: $(SRC)/QuadHolder.cpp $(SRC)/QuadHolder.h $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
$(INTER)/Expression.o: $(SRC)/Expression.cpp $(SRC)/Expression.h $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
$(INTER)/Symbol.o: $(SRC)/Symbol.cpp $(SRC)/Symbol.h $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
$(INTER)/SymbolTable.o: $(SRC)/SymbolTable.cpp $(SRC)/SymbolTable.h $(SRC)/Symbol.h $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
$(INTER)/ParserHandlers.o: $(SRC)/ParserHandlers.cpp $(SRC)/ParserHandlers.h $(SRC)/SymbolTable.h $(SRC)/Symbol.h $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<
$(INTER)/ScannerHandlers.o: $(SRC)/ScannerHandlers.cpp $(SRC)/ScannerHandlers.h $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<

$(INTER)/Parser.o: $(INTER)/Parser.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -Wno-conversion -c -o $@ $<
$(INTER)/Scanner.o: $(INTER)/Scanner.cpp
	$(info Compiling $@)
	@$(CXX) $(CXXFLAGS) -Wno-switch-default -c -o $@ $<

$(INTER)/Parser.cpp: $(SRC_GRAMMAR)/Parser.y $(SRC)/ParserHandlers.h
	$(info Generating $@)
	@$(YACC) $(YFLAGS) --output=$@ --header=$(@:.cpp=.h) $<
$(INTER)/Scanner.cpp: $(SRC_GRAMMAR)/Scanner.l $(SRC)/ScannerHandlers.h $(INTER)/Parser.cpp
	$(info Generating $@)
	@$(LEX) $(LFLAGS) --outfile=$@ --header-file=$(@:.cpp=.h) $<

$(PCH)/pch.h.gch: $(PCH)/pch.h
	$(info Precompiling headers $@)
	@$(CXX) $(CXXFLAGS) -x c++-header -c -o $@ $<

clean:
	$(RM) $(INTER)/*
	$(RM) $(BIN)/*

memcheck:
	valgrind $(VFLAGS) ./$(BIN)/$(TARGET) in.alpha out.txt 2> err.txt
