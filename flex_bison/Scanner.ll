/* Definitions Section */
%top{
    // #include "pch.h"

    // "Top"

}

%{
    #include "Context.h"
    #include "ScannerContext.h"
    #define YY_DECL yy::parser::symbol_type yylex(Context *ctx)

    /* Executed each time a rule is matched, before the action */
    #define YY_USER_ACTION ctx->ScannerCtx()->LocationColumns(yyleng);
%}

%option yylineno

/* no need for use to define yywrap */
%option noyywrap

%option warn

/* do not declare these functions */
%option nodefault nounput noinput nostack nostdinit nomain

/* must be enabled paired together */
%option nounistd never-interactive batch

undefined_char      "\\"|"!"|"@"|"#"|"$"|"^"|"&"|"`"|"~"|"'"|"?"|"|"|("_"[_a-zA-Z0-9]*)

spaces              [ \t]+

newlines            [\r\n]+

keyword             "if"|"else"|"while"|"for"|"function"|"return"|"break"|"continue"|"and"|"not"|"or"|"local"|"true"|"false"|"nil"

operand             "="|"+"|"-"|"*"|"/"|"%"|"=="|"!="|"++"|"--"|">"|"<"|">="|"<="

punctuation         "{"|"}"|"["|"]"|"("|")"|";"|","|":"|"::"|"."|".."

integer             [0-9]+

real                {integer}\.{integer}((E|e){integer})?

id                  [a-zA-Z][_a-zA-Z0-9]*

line_comment        "//".*

no_rule_match       .

%x BLOCK_COMMENT

%x STRING

%%
    /* Rules Section */

%{
    /* Executed whenever the scanning routine is entered */
    ctx->ScannerCtx()->LocationStep();
%}

{spaces}            {ctx->ScannerCtx()->HandleSpaces(yytext, yyleng);}

{newlines}          {ctx->ScannerCtx()->HandleNewlines(yytext, yyleng);}

{undefined_char}    {return ctx->ScannerCtx()->HandleUndefinedChar(yytext, yyleng);}

{keyword}           {return ctx->ScannerCtx()->HandleKeyword(yytext, yyleng);}

{operand}           {return ctx->ScannerCtx()->HandleOperand(yytext, yyleng);}

{punctuation}       {return ctx->ScannerCtx()->HandlePunctuation(yytext, yyleng);}

{integer}           {return ctx->ScannerCtx()->HandleInteger(yytext, yyleng);}

{real}              {return ctx->ScannerCtx()->HandleReal(yytext, yyleng);}

{id}                {return ctx->ScannerCtx()->HandleId(yytext, yyleng);}

{line_comment}      {ctx->ScannerCtx()->HandleLineComment(yytext, yyleng);}

<INITIAL><<EOF>>    {return ctx->ScannerCtx()->HandleEOF(yytext, yyleng);}

\"                  {BEGIN(STRING); ctx->ScannerCtx()->HandleStringBegin(yytext, yyleng);}
<STRING>{
    \"                      {const auto ret = ctx->ScannerCtx()->HandleStringEnd(yytext, yyleng); BEGIN(INITIAL); return ret;}
    \\(a|b|f|n|r|t|v|\\|\") {ctx->ScannerCtx()->HandleStringEscapeChar(yytext, yyleng);}
    \\(.|\n)                {ctx->ScannerCtx()->HandleStringEscapeInvalid(yytext, yyleng);}
    <<EOF>>                 {return ctx->ScannerCtx()->HandleStringUnterminated(yytext, yyleng);}
    [^\\\"\r\n]+            {ctx->ScannerCtx()->HandleStringDefault(yytext, yyleng);}
    [\r\n]+                 {ctx->ScannerCtx()->HandleStringNewlines(yytext, yyleng);}
    .                       {return ctx->ScannerCtx()->HandleNoRuleMatch(yytext, yyleng);}
}

"/*"                {BEGIN(BLOCK_COMMENT); ctx->ScannerCtx()->HandleBlockCommentBegin(yytext, yyleng);}
<BLOCK_COMMENT>{
    "/*"                    {ctx->ScannerCtx()->HandleBlockCommentBegin(yytext, yyleng);}
    (\*)+(\/)               {ctx->ScannerCtx()->HandleBlockCommentEnd(yytext, yyleng); if (ctx->ScannerCtx()->NoPendingBlockComments()) BEGIN(INITIAL);}
    <<EOF>>                 {return ctx->ScannerCtx()->HandleBlockCommentUnterminated(yytext, yyleng);}
    [\r\n]+                 {ctx->ScannerCtx()->HandleBlockCommentNewlines(yytext, yyleng);}
    (\*)+                   {ctx->ScannerCtx()->HandleBlockCommentDefault(yytext, yyleng);/* Eat any trailing '*' */}
    [^/\*\r\n]+             {ctx->ScannerCtx()->HandleBlockCommentDefault(yytext, yyleng);/* Eat anything that is not '/' or '*' or newline */}
    (\/)                    {ctx->ScannerCtx()->HandleBlockCommentDefault(yytext, yyleng);/* Eat backslashes '/' one by one */}
}

{no_rule_match}     {return ctx->ScannerCtx()->HandleNoRuleMatch(yytext, yyleng);}

%%
/* User Code Section */
