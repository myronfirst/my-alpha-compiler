/* Prologue */

%code requires {
    #include "Location.h"
    #include <memory>
    #include <cstddef>
    #include <vector>

    class Symbol;
    class Expression;
    class Statement;
    class CallInfo;
    class ExpressionList;
    class PairList;
    using Label = size_t;
    using FormalArgList = std::vector<const Symbol*>;
    using ExpressionSPtr = std::shared_ptr<Expression>;
    using StatementSPtr = std::shared_ptr<Statement>;
    using CallInfoSPtr = std::shared_ptr<CallInfo>;
    using ExpressionListSPtr = std::shared_ptr<ExpressionList>;
    using PairListSPtr = std::shared_ptr<PairList>;
    using Pair = std::pair<ExpressionSPtr, ExpressionSPtr>;

    struct Context;
}
%code {
    #include "Symbol.h"
    #include "Expression.h"
    #include "Context.h"
    #include "ParserContext.h"

    auto yylex(Context*) -> yy::parser::symbol_type;
}
%param { Context* ctx }

/* Bison Declarations */

%require "3.8.2"
%language "c++"
%skeleton "lalr1.cc"
%locations  /* more accurate error messages */

%define api.value.type variant /* enable C++ invariants */
%define api.value.automove /* enable move semantics in actions, eg. implicitly turn $$=... -> $$=std::move(...) and fun($1) -> fun(std::move($1)) */
%define api.token.constructor /* define make_TOKEN functions */
%define api.token.raw /* disable usage of chars in parsing eg. '+' */
%define api.token.prefix {TOKEN_}
%define api.location.type {Location}
%define parse.assert
%define parse.lac full /* enable Look Ahead Correction to improve error handling */
%define parse.error detailed /* on error messages, report possibly expected tokens when LAC is enabled */

%expect 0

%printer { yyo << $$; } <*>;

%token IF                   "if"
%token ELSE                 "else"
%token WHILE                "while"
%token FOR                  "for"
%token FUNCTION             "function"
%token RETURN               "return"
%token BREAK                "break"
%token CONTINUE             "continue"
%token AND                  "and"
%token NOT                  "not"
%token OR                   "or"
%token LOCAL                "local"
%token TRUE                 "true"
%token FALSE                "false"
%token NIL                  "nil"
%token ASSIGN               "="
%token PLUS                 "+"
%token MINUS                "-"
%token MULTIPLY             "*"
%token DIVIDE               "/"
%token MODULO               "%"
%token EQUAL                "=="
%token NOT_EQUAL            "!="
%token INCREMENT            "++"
%token DECREMENT            "--"
%token GREATER              ">"
%token LESS                 "<"
%token GREATER_EQUAL        ">="
%token LESS_EQUAL           "<="
%token LEFT_CURLY_BRACKET   "{"
%token RIGHT_CURLY_BRACKET  "}"
%token LEFT_SQUARE_BRACKET  "["
%token RIGHT_SQUARE_BRACKET "]"
%token LEFT_PARENTHESIS     "("
%token RIGHT_PARENTHESIS    ")"
%token SEMICOLON            ";"
%token COMMA                ","
%token COLON                ":"
%token DOUBLE_COLON         "::"
%token DOT                  "."
%token DOUBLE_DOT           ".."

/* Precedence line-by-line goes from low to high, Associativity in a single line */

/* %precedence "=" */
/* %right      "=" comment */
%left       "or"
%left       "and"
%nonassoc   "=="    "!="
%nonassoc   ">"     ">="    "<"     "<="
%left       "+"     "-"
%left       "*"     "/"     "%"
%precedence "not"   "--"
/* %right      "not"   "++"    "--" comment */
/* %left       "."     ".." comment */
/* %left       "["     "]" comment */
%precedence ")"
/* %left       "("     ")" comment */
%precedence "else"




%token  <double>        NUMBER
%token  <std::string>   IDENTIFIER
%token  <std::string>   STRING

%nterm                          program
%nterm  <StatementSPtr>         statement_list
%nterm                          block_begin
%nterm                          block_end
%nterm  <StatementSPtr>         statement
%nterm  <StatementSPtr>         non_block_statement
%nterm  <ExpressionSPtr>        expression
%nterm  <ExpressionSPtr>        expression_not_eval
%nterm  <ExpressionSPtr>        equal_prefix
%nterm  <ExpressionSPtr>        not_equal_prefix
%nterm  <ExpressionSPtr>        and_prefix
%nterm  <ExpressionSPtr>        or_prefix
%nterm  <ExpressionSPtr>        operation_expression_not_eval
%nterm  <ExpressionSPtr>        term
%nterm  <ExpressionSPtr>        assign_expression
%nterm  <ExpressionSPtr>        primary
%nterm  <ExpressionSPtr>        lvalue
%nterm  <ExpressionSPtr>        table_item
%nterm  <ExpressionSPtr>        call
%nterm  <CallInfoSPtr>          call_suffix
%nterm  <CallInfoSPtr>          normal_call
%nterm  <CallInfoSPtr>          method_call
%nterm  <ExpressionListSPtr>    expression_list
%nterm  <ExpressionSPtr>        table_definition
%nterm  <PairListSPtr>          pair_list
%nterm  <Pair>                  pair
%nterm  <std::string>           function_name
%nterm  <Symbol*>               function_prefix
%nterm  <FormalArgList>         function_arguments
%nterm                          function_body
%nterm  <const Symbol*>         function_definition
%nterm  <ExpressionSPtr>        constant
%nterm  <FormalArgList>         identifier_list
%nterm                          if
%nterm  <Label>                 if_expression
%nterm  <Label>                 else
%nterm  <StatementSPtr>         if_body
%nterm  <StatementSPtr>         if_statement
%nterm  <Label>                 while
%nterm  <Label>                 while_expression
%nterm  <StatementSPtr>         while_body
%nterm                          while_statement
%nterm  <Label>                 L
%nterm  <Label>                 LJ
%nterm  <Label>                 for
%nterm  <Label>                 for_expression
%nterm  <StatementSPtr>         for_body
%nterm                          for_statement
%nterm                          return_statement

%start  program

%%
/* Grammar Rules */

program:    statement_list                              {bool error=ctx->ParserCtx()->Handle_Program();if(error)YYABORT;};
statement_list:
    %empty                                              {$$=ctx->ParserCtx()->Handle_StatementList_Empty();}
|   statement_list statement                            {$$=ctx->ParserCtx()->Handle_StatementList_NextStatement($1, $2);};

block_begin:    "{"                                     {ctx->ParserCtx()->Handle_BlockBegin();};
block_end:      "}"                                     {ctx->ParserCtx()->Handle_BlockEnd();};
statement:
    non_block_statement                                 {$$=ctx->ParserCtx()->Handle_Statement_NonBlockStatement($1);}
|   block_begin statement_list block_end                {$$=ctx->ParserCtx()->Handle_Statement_StatementList($2);};

non_block_statement:
    expression ";"                                      {$$=ctx->ParserCtx()->Handle_NonBlockStatement_Expression();}
|   if_statement                                        {$$=ctx->ParserCtx()->Handle_NonBlockStatement_IfStatement($1);}
|   while_statement                                     {$$=ctx->ParserCtx()->Handle_NonBlockStatement_WhileStatement();}
|   for_statement                                       {$$=ctx->ParserCtx()->Handle_NonBlockStatement_ForStatement();}
|   return_statement                                    {$$=ctx->ParserCtx()->Handle_NonBlockStatement_ReturnStatement();}
|   "break" ";"                                         {$$=ctx->ParserCtx()->Handle_NonBlockStatement_Break(@1);}
|   "continue" ";"                                      {$$=ctx->ParserCtx()->Handle_NonBlockStatement_Continue(@1);}
|   function_definition                                 {$$=ctx->ParserCtx()->Handle_NonBlockStatement_FunctionDefinition();}
|   ";"                                                 {$$=ctx->ParserCtx()->Handle_NonBlockStatement_Empty();}
;

expression:
    assign_expression                                   {$$=ctx->ParserCtx()->Handle_Expression_AssignExpression($1);}
|   expression_not_eval                                 {$$=ctx->ParserCtx()->Handle_Expression_ShortCircuitEvaluate($1, @1);}
;

expression_not_eval:
    operation_expression_not_eval                       {$$=ctx->ParserCtx()->Handle_Expression_OperationExpressionNotShortCircuitEvaluated($1);}
|   term                                                {$$=ctx->ParserCtx()->Handle_Expression_Term($1);}
;

equal_prefix:       expression_not_eval "=="            {$$=ctx->ParserCtx()->Handle_Equality_Operation_Prefix($1, @1);};
not_equal_prefix:   expression_not_eval "!="            {$$=ctx->ParserCtx()->Handle_Equality_Operation_Prefix($1, @1);};
and_prefix:         expression_not_eval "and"           {$$=ctx->ParserCtx()->Handle_Boolean_Operation_Prefix($1, @1);};
or_prefix:          expression_not_eval "or"            {$$=ctx->ParserCtx()->Handle_Boolean_Operation_Prefix($1, @1);};
operation_expression_not_eval:
    expression_not_eval "+"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Arithmetic_Operation($1, "Add", $3, @1, @3);}
|   expression_not_eval "-"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Arithmetic_Operation($1, "Subtract", $3, @1, @3);}
|   expression_not_eval "*"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Arithmetic_Operation($1, "Multiply", $3, @1, @3);}
|   expression_not_eval "/"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Arithmetic_Operation($1, "Divide", $3, @1, @3);}
|   expression_not_eval "%"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Arithmetic_Operation($1, "Modulo", $3, @1, @3);}
|   expression_not_eval ">"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Relational_Operation($1, "Greater", $3, @1, @3);}
|   expression_not_eval ">="    expression_not_eval     {$$=ctx->ParserCtx()->Handle_Relational_Operation($1, "GreaterEqual",$3, @1, @3);}
|   expression_not_eval "<"     expression_not_eval     {$$=ctx->ParserCtx()->Handle_Relational_Operation($1, "Less", $3, @1, @3);}
|   expression_not_eval "<="    expression_not_eval     {$$=ctx->ParserCtx()->Handle_Relational_Operation($1, "LessEqual", $3, @1, @3);}
|   equal_prefix        expression_not_eval %prec "=="  {$$=ctx->ParserCtx()->Handle_Equality_Operation($1, "Equal", $2, @1, @2);}
|   not_equal_prefix    expression_not_eval %prec "!="  {$$=ctx->ParserCtx()->Handle_Equality_Operation($1, "NotEqual", $2, @1, @2);}
|   and_prefix  L       expression_not_eval %prec "and" {$$=ctx->ParserCtx()->Handle_Boolean_Operation($1, "And", $2, $3, @1, @3);}
|   or_prefix   L       expression_not_eval %prec "or"  {$$=ctx->ParserCtx()->Handle_Boolean_Operation($1, "Or", $2, $3, @1, @3);}
;

term:
    "(" expression ")"                                  {$$=ctx->ParserCtx()->Handle_Term_Expression($2);}
|   "-" expression_not_eval %prec "--"                  {$$=ctx->ParserCtx()->Handle_Term_UnaryMinus_ExpressionNotShortiCircuitEvaluated($2, @2);}
|   "not" expression_not_eval                           {$$=ctx->ParserCtx()->Handle_Term_Not_ExpressionNotShortCircuitEvaluated($2, @2);}
|   "++" lvalue                                         {$$=ctx->ParserCtx()->Handle_Term_UnaryOp_Lvalue("Increment", $2, @2);}
|   lvalue "++"                                         {$$=ctx->ParserCtx()->Handle_Term_Lvalue_UnaryOp($1, "Increment", @1);}
|   "--" lvalue                                         {$$=ctx->ParserCtx()->Handle_Term_UnaryOp_Lvalue("Decrement", $2, @2);}
|   lvalue "--"                                         {$$=ctx->ParserCtx()->Handle_Term_Lvalue_UnaryOp($1,"Decrement", @1);}
|   primary                                             {$$=ctx->ParserCtx()->Handle_Term_Primary($1);}
;

assign_expression:  lvalue "=" expression               {$$=ctx->ParserCtx()->Handle_AssignExpression($1, $3, @1);};

primary:
    lvalue                                              {$$=ctx->ParserCtx()->Handle_Primary_Lvalue($1, @1);}
|   call                                                {$$=ctx->ParserCtx()->Handle_Primary_Call($1);}
|   table_definition                                    {$$=ctx->ParserCtx()->Handle_Primary_TableDefinition($1);}
|   "(" function_definition ")"                         {$$=ctx->ParserCtx()->Handle_Primary_FunctionDefinition($2);}
|   constant                                            {$$=ctx->ParserCtx()->Handle_Primary_Constant($1);}
;

lvalue:
    IDENTIFIER                                          {$$=ctx->ParserCtx()->Handle_Lvalue_Identifier($1, @1);}
|   "local" IDENTIFIER                                  {$$=ctx->ParserCtx()->Handle_Lvalue_Local_Identifier($2, @2);}
|   "::" IDENTIFIER                                     {$$=ctx->ParserCtx()->Handle_Lvalue_Global_Identifier($2, @2);}
|   table_item                                          {$$=ctx->ParserCtx()->Handle_Lvalue_TableItem($1, @1);}
;

table_item:
    lvalue "." IDENTIFIER                               {$$=ctx->ParserCtx()->Handle_TableItem_Lvalue_Identifier($1, $3, @3);}
|   lvalue "[" expression "]"                           {$$=ctx->ParserCtx()->Handle_TableItem_Lvalue_Expression($1, $3, @3);}
|   call "." IDENTIFIER                                 {$$=ctx->ParserCtx()->Handle_TableItem_Call_Identifier($1, $3, @3);}
|   call "[" expression "]"                             {$$=ctx->ParserCtx()->Handle_TableItem_Call_Expression($1, $3, @3);}
;
call:
    call "(" expression_list ")"                        {$$=ctx->ParserCtx()->Handle_Call_NextCall($1, $3, @1);}
|   lvalue call_suffix                                  {$$=ctx->ParserCtx()->Handle_Call_Lvalue_CallSuffix($1, $2, @1);}
|   "(" function_definition ")" "(" expression_list ")" {$$=ctx->ParserCtx()->Handle_Call_FunctionDefinition_ExpressionList($2, $5, @2);}
;
call_suffix:
    normal_call                                         {$$=ctx->ParserCtx()->Handle_CallSuffix_NormalCall($1);}
|   method_call                                         {$$=ctx->ParserCtx()->Handle_CallSuffix_MethodCall($1);}
;
normal_call:    "(" expression_list ")"                 {$$=ctx->ParserCtx()->Handle_NormalCall($2);};
method_call:    ".." IDENTIFIER "(" expression_list ")" {$$=ctx->ParserCtx()->Handle_MethodCall($2, $4);};

expression_list:
    %empty                                              {$$=ctx->ParserCtx()->Handle_ExpressionList_Empty();}
|   expression                                          {$$=ctx->ParserCtx()->Handle_ExpressionList_Expression($1);}
|   expression_list "," expression                      {$$=ctx->ParserCtx()->Handle_ExpressionList_NextExpression($1, $3);}
;

table_definition:
    "[" expression_list "]"                             {$$=ctx->ParserCtx()->Handle_TableDefinition_ExpressionList($2, @2);}
|   "[" pair_list "]"                                   {$$=ctx->ParserCtx()->Handle_TableDefinition_PairList($2, @2);}
;
pair_list:
    pair                                                {$$=ctx->ParserCtx()->Handle_PairList_Pair($1);}
|   pair_list "," pair                                  {$$=ctx->ParserCtx()->Handle_PairList_NextPair($1, $3);}
;
pair:   "{" expression ":" expression "}"               {$$=ctx->ParserCtx()->Handle_Pair($2, $4);};

function_name:
    %empty                                              {$$=ctx->ParserCtx()->Handle_FunctionName();}
|   IDENTIFIER                                          {$$=ctx->ParserCtx()->Handle_FunctionName($1);}
;
function_prefix:        "function" function_name        {$$=ctx->ParserCtx()->Handle_FunctionPrefix($2, @2);};
function_arguments:     "(" identifier_list ")"         {$$=ctx->ParserCtx()->Handle_FunctionArguments($2);};
function_body:          "{" statement_list "}"          {ctx->ParserCtx()->Handle_FunctionBody();};
function_definition:    function_prefix function_arguments function_body
                                                        {$$=ctx->ParserCtx()->Handle_FunctionDefinition($1, $2);};

constant:
    NUMBER                                              {$$=ctx->ParserCtx()->Handle_Constant_Number($1);}
|   STRING                                              {$$=ctx->ParserCtx()->Handle_Constant_String($1);}
|   "nil"                                               {$$=ctx->ParserCtx()->Handle_Constant_Nil();}
|   "true"                                              {$$=ctx->ParserCtx()->Handle_Constant_Boolean(true);}
|   "false"                                             {$$=ctx->ParserCtx()->Handle_Constant_Boolean(false);}
;

identifier_list:
    %empty                                              {$$=ctx->ParserCtx()->Handle_IdentifierList_Empty();}
|   IDENTIFIER                                          {$$=ctx->ParserCtx()->Handle_IdentifierList_Identifier($1, @1);}
|   identifier_list "," IDENTIFIER                      {$$=ctx->ParserCtx()->Handle_IdentifierList_NextIdentifier($1, $3, @3);}
;

if:             "if"                                    {ctx->ParserCtx()->Handle_If();};
if_expression:  "(" expression ")"                      {$$=ctx->ParserCtx()->Handle_IfExpression($2, @2);};
else:           "else"                                  {$$=ctx->ParserCtx()->Handle_Else(@1);};
if_body:
    non_block_statement                                 {$$=ctx->ParserCtx()->Handle_IfBody($1);}
|   "{" statement_list "}"                              {$$=ctx->ParserCtx()->Handle_IfBody($2);}
;
if_statement:
    if if_expression if_body                %prec ")"   {$$=ctx->ParserCtx()->Handle_IfStatement($2, $3);}
|   if if_expression if_body else if_body   %prec "else"{$$=ctx->ParserCtx()->Handle_IfElseStatement($2, $3, $4, $5);}
;

while:              "while"                             {$$=ctx->ParserCtx()->Handle_While();};
while_expression:   "(" expression ")"                  {$$=ctx->ParserCtx()->Handle_WhileExpression($2, @2);};
while_body:
    non_block_statement                                 {$$=ctx->ParserCtx()->Handle_WhileBody($1);}
|   "{" statement_list "}"                              {$$=ctx->ParserCtx()->Handle_WhileBody($2);}
;
while_statement:    while while_expression while_body   {ctx->ParserCtx()->Handle_WhileStatement($1, $2, $3, @3);};

L:              %empty                                  {$$=ctx->ParserCtx()->Handle_Label();};
LJ:             %empty                                  {$$=ctx->ParserCtx()->Handle_LabelJump(Location{});};
for:            "for"                                   {ctx->ParserCtx()->Handle_For();};
for_expression: expression                              {$$=ctx->ParserCtx()->Handle_ForExpression($1, @1);};
for_body:
    non_block_statement                                 {$$=ctx->ParserCtx()->Handle_ForBody($1);}
|   "{" statement_list "}"                              {$$=ctx->ParserCtx()->Handle_ForBody($2);}
;
for_statement:  for "(" expression_list ";" L for_expression ";" LJ expression_list ")" LJ for_body LJ
                                                        {ctx->ParserCtx()->Handle_ForStatement($5, $6, $8, $11, $12, $13);};

return_statement:
    "return" ";"                                        {ctx->ParserCtx()->Handle_ReturnStatement_Empty(@1);}
|   "return" expression ";"                             {ctx->ParserCtx()->Handle_ReturnStatement_Expression($2, @1);}
;

%%
/* Epilogue */
#include <string>
auto yy::parser::error(const Location& loc, const std::string& msg) -> void {
    std::cout << loc << " : " << msg <<'\n';
}
