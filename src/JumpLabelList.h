#ifndef JUMP_LABEL_LIST_H
#define JUMP_LABEL_LIST_H

#include <cstddef>
#include <vector>

using Label = size_t;
struct JumpLabelList : public std::vector<Label> {
    using std::vector<Label>::vector;
    auto operator+=(const JumpLabelList& rhs) -> JumpLabelList&;
    friend auto operator+(JumpLabelList lhs, const JumpLabelList& rhs) -> JumpLabelList;
};

#endif
