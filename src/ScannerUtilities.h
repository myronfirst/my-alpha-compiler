#ifndef SCANNER_UTILITIES_H
#define SCANNER_UTILITIES_H

#include "Location.h"

#include <iosfwd>
#include <string>
#include <vector>

struct Token {
    Location location;
    size_t numId;
    std::string value;
    std::string type;
};
auto operator<<(std::ostream& os, const Token& token) -> std::ostream&;

class TokenHolder {
public:
    auto Insert(const Token& token) -> void;
    auto PrintTo(std::ostream& os) const -> void;

private:
    std::vector<Token> tokens{};
};

#endif
