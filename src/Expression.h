#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "JumpLabelList.h"

#include <memory>
#include <variant>
#include <vector>

struct Symbol;
using Label = size_t;

struct ShortCircuitLists {
    JumpLabelList trueList{};
    JumpLabelList falseList{};
};

enum class ExpressionType {
    Variable,
    TableItem,

    UserFunction,
    LibraryFunction,    // unused

    Arithmetic,
    Boolean,
    Assign,    // unused
    NewTable,

    ConstNumber,
    ConstBoolean,
    ConstString,

    Nil
};

struct Expression {
    using ExpressionSPtr = std::shared_ptr<Expression>;
    using Index = std::shared_ptr<Expression>;
    using ConstNumber = double;
    using ConstBoolean = bool;
    using ConstString = std::string;
    using VariantData = std::variant<ConstBoolean, ConstNumber, ConstString, Index, ShortCircuitLists>;

    static auto Make(ExpressionType type, const Symbol* symbol) -> ExpressionSPtr;
    static auto MakeConstBoolean(ConstBoolean data) -> ExpressionSPtr;
    static auto MakeConstNumber(ConstNumber data) -> ExpressionSPtr;
    static auto MakeConstString(ConstString data) -> ExpressionSPtr;
    static auto MakeNil() -> ExpressionSPtr;
    static auto MakeTableItem(const Symbol* symbol, Index data) -> ExpressionSPtr;
    static auto MakeShortCircuit(ShortCircuitLists data) -> ExpressionSPtr;
    auto GetType() const -> ExpressionType;
    auto GetSymbol() const -> const Symbol*;
    auto GetConstBoolean() const -> ConstBoolean;
    auto GetConstNumber() const -> ConstNumber;
    auto GetConstString() const -> ConstString;
    auto GetIndex() const -> Index;
    auto GetShortCircuitLists() const -> ShortCircuitLists;
    auto IsValidArithmeticArgument() const -> bool;
    auto IsConstBooleanConvertible() -> bool;
    auto GetConstBooleanConversion() -> bool;
    auto IsConst() -> bool;

    ExpressionType type{};
    const Symbol* symbol{};
    VariantData data{};
};
auto operator<<(std::ostream& os, const Expression& obj) -> std::ostream&;
using ExpressionSPtr = Expression::ExpressionSPtr;

struct ExpressionList : public std::vector<ExpressionSPtr> {
    using ExpressionListSPtr = std::shared_ptr<ExpressionList>;
    static auto Make() -> ExpressionListSPtr;
};
using ExpressionListSPtr = ExpressionList::ExpressionListSPtr;

struct CallInfo {
    using CallInfoSPtr = std::shared_ptr<CallInfo>;
    ExpressionListSPtr expressionList{};
    bool isMethodCall{};
    std::string methodName{};
    static auto Make(const ExpressionListSPtr& expressionList, bool isMethodCall, const std::string& methodName) -> CallInfoSPtr;
};
using CallInfoSPtr = CallInfo::CallInfoSPtr;

using Pair = std::pair<ExpressionSPtr, ExpressionSPtr>;
std::ostream& operator<<(std::ostream& os, const Pair& obj);

struct PairList : public std::vector<Pair> {
    using PairListSPtr = std::shared_ptr<PairList>;
    static auto Make() -> PairListSPtr;
};
using PairListSPtr = PairList::PairListSPtr;

struct Statement {
    using StatementSPtr = std::shared_ptr<Statement>;
    JumpLabelList breakList{};
    JumpLabelList continueList{};
    static auto Make() -> StatementSPtr;
    static auto MakeBreak(const JumpLabelList&) -> StatementSPtr;
    static auto MakeContinue(const JumpLabelList&) -> StatementSPtr;
    static auto Merge(const StatementSPtr&, const StatementSPtr&) -> StatementSPtr;
};
using StatementSPtr = Statement::StatementSPtr;

#endif
