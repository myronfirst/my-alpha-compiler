#ifndef PARSER_CONTEXT_H
#define PARSER_CONTEXT_H

#include "Location.h"

#include <memory>
#include <string>
#include <utility>
#include <vector>

class ScopeStackType;
class ScopeSpaceStackType;
class HiddenSymbolGeneratorType;
class SymbolTable;
class QuadHolder;

enum class ScopeSpaceName;
enum class OpCode;

class Symbol;
class Expression;
class Statement;
class CallInfo;
class ExpressionList;
class PairList;

using Label = size_t;
using FormalArgList = std::vector<const Symbol*>;
using ExpressionSPtr = std::shared_ptr<Expression>;
using StatementSPtr = std::shared_ptr<Statement>;
using CallInfoSPtr = std::shared_ptr<CallInfo>;
using ExpressionListSPtr = std::shared_ptr<ExpressionList>;
using PairListSPtr = std::shared_ptr<PairList>;
using Pair = std::pair<ExpressionSPtr, ExpressionSPtr>;

class ParserContext {
private:
    bool parseErrorFlag{ false };
    const bool shortCircuitEvaluationEnabled;
    std::unique_ptr<SymbolTable> symbolTable;
    std::unique_ptr<QuadHolder> quadHolder;
    std::unique_ptr<ScopeStackType> scopeStack;
    std::unique_ptr<ScopeSpaceStackType> scopeSpaceStack;
    std::unique_ptr<HiddenSymbolGeneratorType> hiddenSymbolGenerator;

    auto SemanticError(const Location& location, const std::string& name, const std::string& message) -> void;
    auto CheckFunctionTypeModificationError(const Expression* expression, const Location& loc) -> void;
    auto CheckValidArithmetic(const Expression* lvalue, const Location& loc) -> void;

    friend HiddenSymbolGeneratorType;
    auto HandleScopeBegin(const std::string& scopeName) -> void;
    auto HandleScopeEnd() -> void;
    auto HandleScopeSpaceBegin(ScopeSpaceName scopeSpaceName) -> void;
    auto HandleScopeSpaceEnd() -> void;

    auto ShortCircuitEvaluate(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Equality_Relational_Impl_Default(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr;
    auto Equality_Relational_Impl_ShortCircuit(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr;
    auto Equality_Relational_Impl(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr;

    auto BooleanOperation_Impl_Default(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr;
    auto ShortCircuitTruthTest(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr;
    auto Or_ShortCircuit(const ExpressionSPtr& expressionLeft, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft) -> ExpressionSPtr;
    auto And_ShortCircuit(const ExpressionSPtr& expressionLeft, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft) -> ExpressionSPtr;
    auto BooleanOperation_Impl_ShortCircuit(const ExpressionSPtr& expressionLeft, OpCode op, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr;
    auto BooleanOperation_Impl(const ExpressionSPtr& expressionLeft, OpCode op, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr;

    auto Term_Not_Expression_Impl_Default(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr;
    auto Term_Not_Expression_Impl_ShortCircuit(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr;
    auto Term_Not_Expression_Impl(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr;

public:
    explicit ParserContext(bool);
    auto GetSymbolTable() const -> SymbolTable*;
    auto GetQuadHolder() const -> QuadHolder*;

    auto Handle_Program() -> bool;
    auto Handle_StatementList_Empty() -> StatementSPtr;
    auto HandleError() -> StatementSPtr;
    auto Handle_StatementList_NextStatement(const StatementSPtr&, const StatementSPtr&) -> StatementSPtr;

    auto Handle_BlockBegin() -> void;
    auto Handle_BlockEnd() -> void;
    auto Handle_Statement_NonBlockStatement(const StatementSPtr&) -> StatementSPtr;
    auto Handle_Statement_StatementList(const StatementSPtr&) -> StatementSPtr;

    auto Handle_NonBlockStatement_Expression() -> StatementSPtr;
    auto Handle_NonBlockStatement_IfStatement(const StatementSPtr&) -> StatementSPtr;
    auto Handle_NonBlockStatement_WhileStatement() -> StatementSPtr;
    auto Handle_NonBlockStatement_ForStatement() -> StatementSPtr;
    auto Handle_NonBlockStatement_ReturnStatement() -> StatementSPtr;
    auto Handle_NonBlockStatement_Break(const Location&) -> StatementSPtr;
    auto Handle_NonBlockStatement_Continue(const Location&) -> StatementSPtr;
    auto Handle_NonBlockStatement_FunctionDefinition() -> StatementSPtr;
    auto Handle_NonBlockStatement_Empty() -> StatementSPtr;
    auto Handle_NonBlockStatement_Error() -> StatementSPtr;

    auto Handle_Expression_AssignExpression(const ExpressionSPtr&) -> ExpressionSPtr;
    auto Handle_Expression_ShortCircuitEvaluate(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Expression_OperationExpressionNotShortCircuitEvaluated(const ExpressionSPtr&) -> ExpressionSPtr;
    auto Handle_Expression_Term(const ExpressionSPtr&) -> ExpressionSPtr;

    auto Handle_Equality_Operation_Prefix(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Arithmetic_Operation(const ExpressionSPtr&, const std::string&, const ExpressionSPtr&, const Location&, const Location&) -> ExpressionSPtr;
    auto Handle_Equality_Operation(const ExpressionSPtr&, const std::string&, const ExpressionSPtr&, const Location&, const Location&) -> ExpressionSPtr;
    auto Handle_Relational_Operation(const ExpressionSPtr&, const std::string&, const ExpressionSPtr&, const Location&, const Location&) -> ExpressionSPtr;
    auto Handle_Boolean_Operation_Prefix(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Boolean_Operation(const ExpressionSPtr&, const std::string&, Label, const ExpressionSPtr&, const Location&, const Location&) -> ExpressionSPtr;

    auto Handle_Term_Expression(const ExpressionSPtr&) -> ExpressionSPtr;
    auto Handle_Term_UnaryMinus_ExpressionNotShortiCircuitEvaluated(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Term_Not_ExpressionNotShortCircuitEvaluated(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Term_UnaryOp_Lvalue(const std::string&, const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Term_Lvalue_UnaryOp(const ExpressionSPtr&, const std::string&, const Location&) -> ExpressionSPtr;
    auto Handle_Term_Primary(const ExpressionSPtr&) -> ExpressionSPtr;

    auto Handle_AssignExpression(const ExpressionSPtr&, const ExpressionSPtr&, const Location&) -> ExpressionSPtr;

    auto Handle_Primary_Lvalue(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;
    auto Handle_Primary_Call(const ExpressionSPtr&) -> ExpressionSPtr;
    auto Handle_Primary_TableDefinition(const ExpressionSPtr&) -> ExpressionSPtr;
    auto Handle_Primary_FunctionDefinition(const Symbol*) -> ExpressionSPtr;
    auto Handle_Primary_Constant(const ExpressionSPtr&) -> ExpressionSPtr;

    auto Handle_Lvalue_Identifier(const std::string&, const Location&) -> ExpressionSPtr;
    auto Handle_Lvalue_Local_Identifier(const std::string&, const Location&) -> ExpressionSPtr;
    auto Handle_Lvalue_Global_Identifier(const std::string&, const Location&) -> ExpressionSPtr;
    auto Handle_Lvalue_TableItem(const ExpressionSPtr&, const Location&) -> ExpressionSPtr;

    auto Handle_TableItem_Lvalue_Identifier(const ExpressionSPtr&, const std::string&, const Location& loc) -> ExpressionSPtr;
    auto Handle_TableItem_Lvalue_Expression(const ExpressionSPtr&, const ExpressionSPtr&, const Location& loc) -> ExpressionSPtr;
    auto Handle_TableItem_Call_Identifier(const ExpressionSPtr&, const std::string&, const Location& loc) -> ExpressionSPtr;
    auto Handle_TableItem_Call_Expression(const ExpressionSPtr&, const ExpressionSPtr&, const Location& loc) -> ExpressionSPtr;

    auto Handle_Call_NextCall(const ExpressionSPtr&, const ExpressionListSPtr&, const Location& loc) -> ExpressionSPtr;
    auto Handle_Call_Lvalue_CallSuffix(const ExpressionSPtr&, const CallInfoSPtr&, const Location& loc) -> ExpressionSPtr;
    auto Handle_Call_FunctionDefinition_ExpressionList(const Symbol*, const ExpressionListSPtr&, const Location& loc) -> ExpressionSPtr;
    auto Handle_CallSuffix_NormalCall(const CallInfoSPtr&) -> CallInfoSPtr;
    auto Handle_CallSuffix_MethodCall(const CallInfoSPtr&) -> CallInfoSPtr;
    auto Handle_NormalCall(const ExpressionListSPtr&) -> CallInfoSPtr;
    auto Handle_MethodCall(const std::string&, const ExpressionListSPtr&) -> CallInfoSPtr;

    auto Handle_ExpressionList_Empty() -> ExpressionListSPtr;
    auto Handle_ExpressionList_Expression(const ExpressionSPtr&) -> ExpressionListSPtr;
    auto Handle_ExpressionList_NextExpression(const ExpressionListSPtr&, const ExpressionSPtr&) -> ExpressionListSPtr;

    auto Handle_TableDefinition_ExpressionList(const ExpressionListSPtr&, const Location& loc) -> ExpressionSPtr;
    auto Handle_TableDefinition_PairList(const PairListSPtr&, const Location& loc) -> ExpressionSPtr;
    auto Handle_PairList_Pair(const Pair&) -> PairListSPtr;
    auto Handle_PairList_NextPair(const PairListSPtr&, const Pair&) -> PairListSPtr;
    auto Handle_Pair(const ExpressionSPtr&, const ExpressionSPtr&) -> Pair;

    auto Handle_FunctionName(const std::string& = {}) -> std::string;
    auto Handle_FunctionPrefix(const std::string&, const Location&) -> Symbol*;
    auto Handle_FunctionArguments(const FormalArgList&) -> FormalArgList;
    auto Handle_FunctionBody() -> void;
    auto Handle_FunctionDefinition(Symbol*, const FormalArgList&) -> const Symbol*;

    auto Handle_Constant_Number(double) -> ExpressionSPtr;
    auto Handle_Constant_String(const std::string&) -> ExpressionSPtr;
    auto Handle_Constant_Nil() -> ExpressionSPtr;
    auto Handle_Constant_Boolean(bool) -> ExpressionSPtr;

    auto Handle_IdentifierList_Empty() -> FormalArgList;
    auto Handle_IdentifierList_Identifier(const std::string&, const Location&) -> FormalArgList;
    auto Handle_IdentifierList_NextIdentifier(FormalArgList, const std::string&, const Location&) -> FormalArgList;
    auto Handle_IdentifierList_NextError(FormalArgList) -> FormalArgList;

    auto Handle_If() -> void;
    auto Handle_IfExpression(const ExpressionSPtr&, const Location&) -> Label;
    auto Handle_Else(const Location&) -> Label;
    auto Handle_IfBody(const StatementSPtr&) -> StatementSPtr;
    auto Handle_IfStatement(Label, const StatementSPtr&) -> StatementSPtr;
    auto Handle_IfElseStatement(Label, const StatementSPtr&, Label, const StatementSPtr&) -> StatementSPtr;

    auto Handle_While() -> Label;
    auto Handle_WhileExpression(const ExpressionSPtr&, const Location&) -> Label;
    auto Handle_WhileBody(const StatementSPtr&) -> StatementSPtr;
    auto Handle_WhileStatement(Label, Label, const StatementSPtr&, const Location&) -> void;

    auto Handle_Label() -> Label;
    auto Handle_LabelJump(const Location& loc) -> Label;
    auto Handle_For() -> void;
    auto Handle_ForExpression(const ExpressionSPtr&, const Location&) -> Label;
    auto Handle_ForBody(const StatementSPtr&) -> StatementSPtr;
    auto Handle_ForStatement(Label, Label, Label, Label, const StatementSPtr&, Label) -> void;

    auto Handle_ReturnStatement_Empty(const Location&) -> void;
    auto Handle_ReturnStatement_Expression(const ExpressionSPtr&, const Location&) -> void;
};
#endif
