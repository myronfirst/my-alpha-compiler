#ifndef SCOPE_SPACE_H
#define SCOPE_SPACE_H

enum class ScopeSpaceName {
    ProgramVariables,
    FunctionLocals,
    FormalArguments
};
struct ScopeSpace {
    using Offset = unsigned long;
    ScopeSpaceName name{};
    Offset offset{};
};

#endif
