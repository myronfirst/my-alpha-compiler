// #include "pch.h"

#include "Expression.h"

#include "JumpLabelList.h"
#include "Symbol.h"

#include <iomanip>
#include <memory>
#include <sstream>
#include <variant>
#include <vector>

#include <cassert>

namespace {
    [[maybe_unused]] auto TypeToString(ExpressionType type) -> std::string {
        switch (type) {
            case ExpressionType::Variable: return "Variable";
            case ExpressionType::TableItem: return "TableItem";
            case ExpressionType::UserFunction: return "UserFunction";
            case ExpressionType::LibraryFunction: return "LibraryFunction";
            case ExpressionType::Arithmetic: return "Arithmetic";
            case ExpressionType::Boolean: return "Boolean";
            case ExpressionType::Assign: return "Assign";
            case ExpressionType::NewTable: return "NewTable";
            case ExpressionType::ConstNumber: return "ConstNumber";
            case ExpressionType::ConstBoolean: return "ConstBoolean";
            case ExpressionType::ConstString: return "ConstString";
            case ExpressionType::Nil: return "Nil";
            default: assert(false); return {};
        }
    }
}    // namespace

auto Expression::Make(ExpressionType type, const Symbol* symbol) -> ExpressionSPtr { return std::make_shared<Expression>(type, symbol, VariantData{}); }
auto Expression::MakeConstBoolean(bool data) -> ExpressionSPtr { return std::make_shared<Expression>(ExpressionType::ConstBoolean, nullptr, data); }
auto Expression::MakeConstNumber(double data) -> ExpressionSPtr { return std::make_shared<Expression>(ExpressionType::ConstNumber, nullptr, data); }
auto Expression::MakeConstString(std::string data) -> ExpressionSPtr { return std::make_shared<Expression>(ExpressionType::ConstString, nullptr, data); }
auto Expression::MakeNil() -> ExpressionSPtr { return std::make_shared<Expression>(ExpressionType::Nil, nullptr, VariantData{}); }
auto Expression::MakeTableItem(const Symbol* symbol, Index data) -> ExpressionSPtr { return std::make_shared<Expression>(ExpressionType::TableItem, symbol, data); }
auto Expression::MakeShortCircuit(ShortCircuitLists data) -> ExpressionSPtr { return std::make_shared<Expression>(ExpressionType::Boolean, nullptr, data); }

auto Expression::GetType() const -> ExpressionType { return type; }
auto Expression::GetSymbol() const -> const Symbol* { return symbol; }
auto Expression::GetConstBoolean() const -> bool {
    assert(type == ExpressionType::ConstBoolean);
    assert(std::holds_alternative<bool>(data));
    return std::get<bool>(data);
}
auto Expression::GetConstNumber() const -> double {
    assert(type == ExpressionType::ConstNumber);
    assert(std::holds_alternative<double>(data));
    return std::get<double>(data);
}
auto Expression::GetConstString() const -> std::string {
    assert(type == ExpressionType::ConstString);
    assert(std::holds_alternative<std::string>(data));
    return std::get<std::string>(data);
}
auto Expression::GetIndex() const -> ExpressionSPtr {
    assert(type == ExpressionType::TableItem);
    assert(std::holds_alternative<Index>(data));
    return std::get<Index>(data);
}
auto Expression::GetShortCircuitLists() const -> ShortCircuitLists {
    assert(type == ExpressionType::Boolean);
    assert(std::holds_alternative<ShortCircuitLists>(data));
    return std::get<ShortCircuitLists>(data);
}
auto Expression::IsValidArithmeticArgument() const -> bool {
    switch (type) {
        case ExpressionType::Variable:
        case ExpressionType::TableItem:
        case ExpressionType::Arithmetic:
        case ExpressionType::Assign:
        case ExpressionType::ConstNumber: return true;
        case ExpressionType::UserFunction:
        case ExpressionType::LibraryFunction:
        case ExpressionType::Boolean:
        case ExpressionType::NewTable:
        case ExpressionType::ConstBoolean:
        case ExpressionType::ConstString:
        case ExpressionType::Nil: return false;
        default: assert(false); return {};
    }
}

auto Expression::IsConstBooleanConvertible() -> bool {
    switch (type) {
        case ExpressionType::TableItem:
        case ExpressionType::ConstNumber:
        case ExpressionType::UserFunction:
        case ExpressionType::LibraryFunction:
        case ExpressionType::NewTable:
        case ExpressionType::ConstBoolean:
        case ExpressionType::ConstString:
        case ExpressionType::Nil: return true;
        case ExpressionType::Variable:
        case ExpressionType::Arithmetic:
        case ExpressionType::Assign:
        case ExpressionType::Boolean: return false;
        default: assert(false); return {};
    }
}
auto Expression::GetConstBooleanConversion() -> bool {
    assert(IsConstBooleanConvertible());
    switch (type) {
        case ExpressionType::ConstNumber: return GetConstNumber() > 0.0 || GetConstNumber() < 0.0;
        case ExpressionType::ConstBoolean: return GetConstBoolean();
        case ExpressionType::ConstString: return !GetConstString().empty();
        case ExpressionType::TableItem:
        case ExpressionType::UserFunction:
        case ExpressionType::LibraryFunction:
        case ExpressionType::NewTable: return true;
        case ExpressionType::Nil: return false;
        default: assert(false); return {};
    }
}
auto Expression::IsConst() -> bool { return IsConstBooleanConvertible(); }

auto ExpressionList::Make() -> ExpressionListSPtr {
    return std::make_shared<ExpressionList>();
}

auto PairList::Make() -> PairListSPtr {
    return std::make_shared<PairList>();
}

auto CallInfo::Make(const ExpressionListSPtr& expressionList, bool isMethodCall, const std::string& methodName) -> CallInfoSPtr {
    return std::make_shared<CallInfo>(expressionList, isMethodCall, methodName);
}

std::ostream& operator<<(std::ostream& os, const Pair& obj) {
    os << '{' << *obj.first << ", " << *obj.second << '}';
    return os;
}

auto Statement::Make() -> StatementSPtr {
    return std::make_shared<Statement>();
}
auto Statement::MakeBreak(const JumpLabelList& breakList) -> StatementSPtr {
    return std::make_shared<Statement>(breakList, JumpLabelList{});
}
auto Statement::MakeContinue(const JumpLabelList& continueList) -> StatementSPtr {
    return std::make_shared<Statement>(JumpLabelList{}, continueList);
}
auto Statement::Merge(const StatementSPtr& lhs, const StatementSPtr& rhs) -> StatementSPtr {
    return std::make_shared<Statement>(Statement{ lhs->breakList + rhs->breakList, lhs->continueList + rhs->continueList });
}

namespace {

    template<class... Ts>
    struct overloaded : Ts... {
        using Ts::operator()...;
    };
}    // namespace
auto operator<<(std::ostream& os, const Expression& obj) -> std::ostream& {
    std::stringstream ss{};
    (obj.symbol) ? (ss << obj.symbol->name << ' ') : (ss << "");
    const auto symbolName = ss.str();
    ss.str("");
    switch (obj.type) {
        case ExpressionType::Nil: ss << "nil" << ' '; break;
        case ExpressionType::TableItem:
        case ExpressionType::ConstNumber:
        case ExpressionType::ConstBoolean:
        case ExpressionType::ConstString:
            std::visit(overloaded{
                           [&ss](const Expression::Index& index) { ss << '{' << *index << '}' << ' '; },
                           [&ss](Expression::ConstNumber number) { ss << number << ' '; },
                           [&ss](Expression::ConstBoolean boolean) { ss << std::quoted((boolean ? "true" : "false"), '\'')
                                                                        << ' '; },
                           [&ss](const Expression::ConstString& string) { ss << std::quoted(string) << ' '; },
                           [&ss](const ShortCircuitLists&) {} },
                       obj.data);
            break;
        default: ss << ""; break;
    }
    const auto typedData = ss.str();
    ss.str("");
#ifdef RELEASE_PRINTS
    switch (obj.type) {
        case ExpressionType::Nil: ss << "nil" << ' '; break;
        case ExpressionType::ConstNumber:
        case ExpressionType::ConstBoolean:
        case ExpressionType::ConstString:
            ss << typedData;
            break;
        default: ss << symbolName; break;
    }
#else
    ss << symbolName
       << typedData
       << TypeToString(obj.type);
#endif

    os << ss.str();
    return os;
}
