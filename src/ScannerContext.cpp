#include "ScannerContext.h"

#include "Location.h"
#include "ScannerUtilities.h"

#include "Parser.h"

#include <iomanip>
#include <map>
#include <memory>
#include <span>
#include <sstream>
#include <stack>

ScannerContext::ScannerContext() : tokenHolder{ std::make_unique<TokenHolder>() } {}
auto ScannerContext::GetTokenHolder() const -> TokenHolder* { return tokenHolder.get(); }

#define TO_LAMBDA_LOC(f) ([](const auto& loc) { return f(loc); })
std::map<std::string, ScannerContext::MakeTokenFunction> ScannerContext::NonSemanticTokensCtorMap{
    { "if", TO_LAMBDA_LOC(yy::parser::make_IF) },
    { "else", TO_LAMBDA_LOC(yy::parser::make_ELSE) },
    { "while", TO_LAMBDA_LOC(yy::parser::make_WHILE) },
    { "for", TO_LAMBDA_LOC(yy::parser::make_FOR) },
    { "function", TO_LAMBDA_LOC(yy::parser::make_FUNCTION) },
    { "return", TO_LAMBDA_LOC(yy::parser::make_RETURN) },
    { "break", TO_LAMBDA_LOC(yy::parser::make_BREAK) },
    { "continue", TO_LAMBDA_LOC(yy::parser::make_CONTINUE) },
    { "and", TO_LAMBDA_LOC(yy::parser::make_AND) },
    { "not", TO_LAMBDA_LOC(yy::parser::make_NOT) },
    { "or", TO_LAMBDA_LOC(yy::parser::make_OR) },
    { "local", TO_LAMBDA_LOC(yy::parser::make_LOCAL) },
    { "true", TO_LAMBDA_LOC(yy::parser::make_TRUE) },
    { "false", TO_LAMBDA_LOC(yy::parser::make_FALSE) },
    { "nil", TO_LAMBDA_LOC(yy::parser::make_NIL) },
    { "=", TO_LAMBDA_LOC(yy::parser::make_ASSIGN) },
    { "+", TO_LAMBDA_LOC(yy::parser::make_PLUS) },
    { "-", TO_LAMBDA_LOC(yy::parser::make_MINUS) },
    { "*", TO_LAMBDA_LOC(yy::parser::make_MULTIPLY) },
    { "/", TO_LAMBDA_LOC(yy::parser::make_DIVIDE) },
    { "%", TO_LAMBDA_LOC(yy::parser::make_MODULO) },
    { "==", TO_LAMBDA_LOC(yy::parser::make_EQUAL) },
    { "!=", TO_LAMBDA_LOC(yy::parser::make_NOT_EQUAL) },
    { "++", TO_LAMBDA_LOC(yy::parser::make_INCREMENT) },
    { "--", TO_LAMBDA_LOC(yy::parser::make_DECREMENT) },
    { ">", TO_LAMBDA_LOC(yy::parser::make_GREATER) },
    { "<", TO_LAMBDA_LOC(yy::parser::make_LESS) },
    { ">=", TO_LAMBDA_LOC(yy::parser::make_GREATER_EQUAL) },
    { "<=", TO_LAMBDA_LOC(yy::parser::make_LESS_EQUAL) },
    { "{", TO_LAMBDA_LOC(yy::parser::make_LEFT_CURLY_BRACKET) },
    { "}", TO_LAMBDA_LOC(yy::parser::make_RIGHT_CURLY_BRACKET) },
    { "[", TO_LAMBDA_LOC(yy::parser::make_LEFT_SQUARE_BRACKET) },
    { "]", TO_LAMBDA_LOC(yy::parser::make_RIGHT_SQUARE_BRACKET) },
    { "(", TO_LAMBDA_LOC(yy::parser::make_LEFT_PARENTHESIS) },
    { ")", TO_LAMBDA_LOC(yy::parser::make_RIGHT_PARENTHESIS) },
    { ";", TO_LAMBDA_LOC(yy::parser::make_SEMICOLON) },
    { ",", TO_LAMBDA_LOC(yy::parser::make_COMMA) },
    { ":", TO_LAMBDA_LOC(yy::parser::make_COLON) },
    { "::", TO_LAMBDA_LOC(yy::parser::make_DOUBLE_COLON) },
    { ".", TO_LAMBDA_LOC(yy::parser::make_DOT) },
    { "..", TO_LAMBDA_LOC(yy::parser::make_DOUBLE_DOT) }
};
#undef TO_LAMBDA_LOC

std::map<std::string, std::string> ScannerContext::LexicalTokenTypeMap{
    { "if", "KEYWORD_IF" },
    { "else", "KEYWORD_ELSE" },
    { "while", "KEYWORD_WHILE" },
    { "for", "KEYWORD_FOR" },
    { "function", "KEYWORD_FUNCTION" },
    { "return", "KEYWORD_RETURN" },
    { "break", "KEYWORD_BREAK" },
    { "continue", "KEYWORD_CONTINUE" },
    { "and", "KEYWORD_AND" },
    { "not", "KEYWORD_NOT" },
    { "or", "KEYWORD_OR" },
    { "local", "KEYWORD_LOCAL" },
    { "true", "KEYWORD_TRUE" },
    { "false", "KEYWORD_FALSE" },
    { "nil", "KEYWORD_NIL" },
    { "=", "OPERAND_ASSIGN" },
    { "+", "OPERAND_PLUS" },
    { "-", "OPERAND_MINUS" },
    { "*", "OPERAND_MULTIPLY" },
    { "/", "OPERAND_DIVIDE" },
    { "%", "OPERAND_MODULO" },
    { "==", "OPERAND_EQUAL" },
    { "!=", "OPERAND_NOT_EQUAL" },
    { "++", "OPERAND_UNARY_PLUS" },
    { "--", "OPERAND_UNARY_MINUS" },
    { ">", "OPERAND_GREATER" },
    { "<", "OPERAND_LESS" },
    { ">=", "OPERAND_GREATER_EQUAL" },
    { "<=", "OPERAND_LESS_EQUAL" },
    { "{", "PUNCTUATION_LEFT_CURLY_BRACKET" },
    { "}", "PUNCTUATION_RIGHT_CURLY_BRACKET" },
    { "[", "PUNCTUATION_LEFT_SQUARE_BRACKET" },
    { "]", "PUNCTUATION_RIGHT_SQUARE_BRACKET" },
    { "(", "PUNCTUATION_LEFT_PARENTHESIS" },
    { ")", "PUNCTUATION_RIGHT_PARENTHESIS" },
    { ";", "PUNCTUATION_SEMICOLON" },
    { ",", "PUNCTUATION_COMMA" },
    { ":", "PUNCTUATION_COLON" },
    { "::", "PUNCTUATION_DOUBLE_COLON" },
    { ".", "PUNCTUATION_DOT" },
    { "..", "PUNCTUATION_DOUBLE_DOT" }
};

/* Interface Implementation */

auto ScannerContext::LocationColumns(int length) -> void { locationCursor.Columns(length); }
auto ScannerContext::LocationStep() -> void { locationCursor.Step(); }
auto ScannerContext::NoPendingBlockComments() const -> bool { return noPendingBlockComments; }

auto ScannerContext::HandleSpaces(const char* text, int length) -> void {
    (void)text;
    (void)length;
    locationCursor.Step();
}
auto ScannerContext::HandleNewlines(const char* text, int length) -> void {
    (void)text;
    locationCursor.Lines(length);
    locationCursor.Step();
}
auto ScannerContext::HandleUndefinedChar(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    std::cout << "Undefined Char " << text << " at location " << locationCursor << '\n';
    return yy::parser::make_YYUNDEF(locationCursor);
}
auto ScannerContext::HandleKeyword(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, LexicalTokenTypeMap.at(text) });
    return NonSemanticTokensCtorMap.at(text)(locationCursor);
}
auto ScannerContext::HandleOperand(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, LexicalTokenTypeMap.at(text) });
    return NonSemanticTokensCtorMap.at(text)(locationCursor);
}
auto ScannerContext::HandlePunctuation(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, LexicalTokenTypeMap.at(text) });
    return NonSemanticTokensCtorMap.at(text)(locationCursor);
}
auto ScannerContext::HandleInteger(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, "INTEGER_CONSTANT" });
    return yy::parser::make_NUMBER(std::stod(text), locationCursor);
}
auto ScannerContext::HandleReal(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, "REAL_CONSTANT" });
    return yy::parser::make_NUMBER(std::stod(text), locationCursor);
}
auto ScannerContext::HandleId(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, "IDENTIFIER" });
    return yy::parser::make_IDENTIFIER(text, locationCursor);
}
auto ScannerContext::HandleLineComment(const char* text, int length) -> void {
    (void)length;
    tokenHolder->Insert(Token{ locationCursor, tokenNum++, text, "LINE_COMMENT" });
    locationCursor.Step();
}
auto ScannerContext::HandleEOF(const char* text, int length) -> yy::parser::symbol_type {
    (void)text;
    (void)length;
    return yy::parser::make_YYEOF(locationCursor);
}
auto ScannerContext::HandleNoRuleMatch(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    std::cout << "Non ASCII INPUT " << text << " at location " << locationCursor << '\n';
    return yy::parser::make_YYUNDEF(locationCursor);
}

auto ScannerContext::HandleStringBegin(const char* text, int length) -> void {
    (void)text;
    (void)length;
    stringBuffer.clear();
    stringLocationBegin = locationCursor;
    locationCursor.Step();
}
auto ScannerContext::HandleStringEnd(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    (void)text;
    std::stringstream ss{};
    ss << ' ' << stringLocationBegin << "->" << locationCursor;
    tokenHolder->Insert(Token{ stringLocationBegin, tokenNum++, stringBuffer, "STRING" + ss.str() });
    return yy::parser::make_STRING(stringBuffer, locationCursor);
}
auto ScannerContext::HandleStringEscapeChar(const char* text, int length) -> void {
    (void)length;
    char c = '\0';
    switch (std::span{ text, 2 }[1]) {
        case 'a': c = '\a'; break;
        case 'b': c = '\b'; break;
        case 'f': c = '\f'; break;
        case 'n': c = '\n'; break;
        case 'r': c = '\r'; break;
        case 't': c = '\t'; break;
        case 'v': c = '\v'; break;
        case '\\': c = '\\'; break;
        case '\"': c = '\"'; break;
        default: assert(false);
    };
    stringBuffer.push_back(c);
    locationCursor.Step();
}
auto ScannerContext::HandleStringEscapeInvalid(const char* text, int length) -> void {
    (void)length;
    std::cout << "Invalid String Escape Character " << text << " at location " << locationCursor << '\n';
    stringBuffer.append(text);
    locationCursor.Step();
}
auto ScannerContext::HandleStringUnterminated(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    std::cout << "Unterminated String " << text << " at location " << locationCursor << ", returning EOF\n";
    return yy::parser::make_YYEOF(locationCursor);
}
auto ScannerContext::HandleStringDefault(const char* text, int length) -> void {
    (void)length;
    stringBuffer.append(text);
    locationCursor.Step();
}
auto ScannerContext::HandleStringNewlines(const char* text, int length) -> void {
    (void)text;
    stringBuffer.append(text);
    locationCursor.Lines(length);
    locationCursor.Step();
}

auto ScannerContext::HandleBlockCommentBegin(const char* text, int length) -> void {
    (void)text;
    (void)length;
    blockCommentLocationBeginStack.push(locationCursor);
    noPendingBlockComments = false;
    locationCursor.Step();
}
auto ScannerContext::HandleBlockCommentEnd(const char* text, int length) -> void {
    (void)text;
    (void)length;
    const auto blockCommentLocationBegin = blockCommentLocationBeginStack.top();
    blockCommentLocationBeginStack.pop();
    std::string nestedStr = blockCommentLocationBeginStack.empty() ? "" : "NESTED_";
    std::stringstream ss{};
    ss << nestedStr << "BLOCK_COMMENT " << blockCommentLocationBegin << "->" << locationCursor;
    tokenHolder->Insert(Token{ blockCommentLocationBegin, tokenNum++, "", ss.str() });
    if (blockCommentLocationBeginStack.empty()) noPendingBlockComments = true;
    locationCursor.Step();
}
auto ScannerContext::HandleBlockCommentUnterminated(const char* text, int length) -> yy::parser::symbol_type {
    (void)length;
    std::cout << "Unterminated Block Comment " << text << " at location " << locationCursor << ", returning EOF\n";
    return yy::parser::make_YYEOF(locationCursor);
}
auto ScannerContext::HandleBlockCommentNewlines(const char* text, int length) -> void {
    (void)text;
    locationCursor.Lines(length);
    locationCursor.Step();
}
auto ScannerContext::HandleBlockCommentDefault(const char* text, int length) -> void {
    (void)text;
    (void)length;
    locationCursor.Step();
}
