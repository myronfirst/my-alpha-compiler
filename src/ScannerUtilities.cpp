#include "ScannerUtilities.h"

#include <iomanip>
#include <ostream>
#include <sstream>

auto operator<<(std::ostream& os, const Token& token) -> std::ostream& {
    constexpr auto LocationWidth = 10;
    constexpr auto NumIdWidth = 10;
    constexpr auto ValueWidth = 20;
    constexpr auto TypeWidth = 40;
    constexpr auto CellFill = ' ';
    std::stringstream ss;
    ss << token.location;
    const auto tokenLocation = ss.str();
    ss.str("");
    ss << "#" << token.numId;
    const auto tokenNumId = ss.str();
    ss.str("");
    os << std::left << std::setw(LocationWidth) << std::setfill(CellFill) << tokenLocation
       << std::left << std::setw(NumIdWidth) << std::setfill(CellFill) << tokenNumId
       << std::left << std::setw(ValueWidth) << std::setfill(CellFill) << token.value
       << std::left << std::setw(TypeWidth) << std::setfill(CellFill) << token.type;
    return os;
}
auto TokenHolder::Insert(const Token& token) -> void { tokens.emplace_back(token); }

auto TokenHolder::PrintTo(std::ostream& os) const -> void {
    for (const auto& token : tokens)
        os << token << '\n';
};
