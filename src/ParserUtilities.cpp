#include "ParserUtilities.h"

#include "Symbol.h"
#include "SymbolTable.h"

#include <cassert>
#include <stack>
#include <string>

auto ScopeStackType::Push(const std::string& name) -> void { stack.emplace_back(name); }
auto ScopeStackType::Pop() -> void { stack.pop_back(); }
auto ScopeStackType::GetSize() const -> size_t { return std::ranges::size(stack); }
auto ScopeStackType::IsFunctionName(const std::string& name) -> bool { return name.at(0) != '#'; }
auto ScopeStackType::IsLoopName(const std::string& name) -> bool { return name == LoopScopeName; }
auto ScopeStackType::GetScopeName() const -> std::string {
    assert(!std::ranges::empty(stack));
    return stack.back();
}
auto ScopeStackType::GetScopeNum() const -> size_t {
    assert(!std::ranges::empty(stack));
    return std::ranges::size(stack) - 1;
}
auto ScopeStackType::IsScopeGlobal() const -> bool {
    assert(!std::ranges::empty(stack));
    return stack.back() == GlobalScopeName;
}
auto ScopeStackType::IsScopeBlock() const -> bool {
    assert(!std::ranges::empty(stack));
    return stack.back() == BlockScopeName;
}
auto ScopeStackType::IsScopeIf() const -> bool {
    assert(!std::ranges::empty(stack));
    return stack.back() == IfScopeName;
}
auto ScopeStackType::IsScopeLoop() const -> bool {
    assert(!std::ranges::empty(stack));
    return stack.back() == LoopScopeName;
}
auto ScopeStackType::IsScopeLoopEnclosed() const -> bool {
    assert(!std::ranges::empty(stack));
    for (auto it = std::rbegin(stack); it != std::rend(stack); ++it) {
        if (IsFunctionName(*it)) return false;
        if (IsLoopName(*it)) return true;
    }
    return false;
}
auto ScopeStackType::IsScopeFunction() const -> bool {
    assert(!std::ranges::empty(stack));
    return IsFunctionName(stack.back());
}
auto ScopeStackType::IsScopeFunctionEnclosed() const -> bool {
    assert(!std::ranges::empty(stack));
    for (auto it = rbegin(stack); it != std::rend(stack); ++it)
        if (IsFunctionName(*it)) return true;
    return false;
}
auto ScopeStackType::IsSymbolOutsideEnclosingFunction(Scope scope) const -> bool {
    for (Scope curr = GetScopeNum(); curr > scope; --curr)
        if (IsFunctionName(stack.at(curr))) return true;

    return false;
}

auto ScopeSpaceStackType::Push(ScopeSpace scopeSpace) -> void { stack.emplace_back(scopeSpace); }
auto ScopeSpaceStackType::Pop() -> void { stack.pop_back(); }
auto ScopeSpaceStackType::GetSize() const -> size_t { return std::ranges::size(stack); }
auto ScopeSpaceStackType::GetScopeSpace() const -> ScopeSpace {
    assert(!std::ranges::empty(stack));
    return stack.back();
}
auto ScopeSpaceStackType::GetOffset() const -> Offset { return stack.back().offset; }
auto ScopeSpaceStackType::IncrementOffset(Offset val) -> void { stack.back().offset += val; }

HiddenSymbolGeneratorType::HiddenSymbolGeneratorType(SymbolTable* symbolTable_,
                                                     ScopeStackType* scopeStack_,
                                                     ScopeSpaceStackType* scopeSpaceStack_)
    : symbolTable{ symbolTable_ },
      scopeStack{ scopeStack_ },
      scopeSpaceStack{ scopeSpaceStack_ } {}

auto HiddenSymbolGeneratorType::GetUserFunctionName() -> std::string {
    auto name = UserFunctionPrefix + std::to_string(userFunctionCounter);
    userFunctionCounter += 1;
    return name;
}
auto HiddenSymbolGeneratorType::ResetTemporaryVariableNames() -> void { temporaryVariableCounter = 0UL; }
auto HiddenSymbolGeneratorType::GetTemporaryVariable(const Location& loc) -> const Symbol* {
    const auto name = GetTemporaryVariableName();
    const Symbol* existingSymbol = symbolTable->LookUpAtScope(name, scopeStack->GetScopeNum());
    if (existingSymbol == nullptr) {
        const Symbol* tempSymbol = symbolTable->Insert({ SymbolType::TempVar, name, scopeStack->GetScopeNum(), ScopeSpace{}, loc });
        assert(scopeSpaceStack->GetScopeSpace().name == ScopeSpaceName::ProgramVariables || scopeSpaceStack->GetScopeSpace().name == ScopeSpaceName::FunctionLocals);
        scopeSpaceStack->IncrementOffset();
        return tempSymbol;
    }
    assert(existingSymbol->type == SymbolType::TempVar);
    return existingSymbol;
}
auto HiddenSymbolGeneratorType::RecycleOrGetTemporaryVariable(std::vector<const Symbol*> symbolList, const Location& loc) -> const Symbol* {
    for (const auto& symbol : symbolList)
        if (symbol && IsTemporaryVariableName(symbol->name)) return symbol;
    return GetTemporaryVariable(loc);
}

auto HiddenSymbolGeneratorType::GetTemporaryVariableName() -> std::string {
    auto name = TemporaryVariablePrefix + std::to_string(temporaryVariableCounter);
    temporaryVariableCounter += 1;
    return name;
}
auto HiddenSymbolGeneratorType::IsTemporaryVariableName(const std::string& name) -> bool {
    return name.starts_with(TemporaryVariablePrefix);
}
