#ifndef LOCATION_H
#define LOCATION_H

#include <iosfwd>

struct Position {
    using Counter = int;
    auto Lines(Counter count = 1) -> void;
    auto Columns(Counter count = 1) -> void;

    Counter line{ 1 };
    Counter column{ 1 };
};
Position& operator+=(Position& res, Position::Counter width);
auto operator<<(std::ostream& os, const Position& pos) -> std::ostream&;

struct Location {
    using Counter = Position::Counter;
    auto Lines(Counter count = 1) -> void;
    auto Columns(Counter count = 1) -> void;
    auto Step() -> void;

    Position begin{};
    Position end{};
};
auto operator<<(std::ostream& os, const Location& loc) -> std::ostream&;

#endif
