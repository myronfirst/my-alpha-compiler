#include "gtest/gtest.h"

#include "Compiler.h"

#include <algorithm>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <ranges>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>

namespace {
    constexpr std::string_view TestFolderName = "test/phase3";
    constexpr std::string_view InputSuffix = ".input.alpha";
    constexpr std::string_view OutputQuadsSuffix = ".result.quads";
    constexpr std::string_view ExpectedQuadsSuffix = ".expected.quads";
}    // namespace

class CompilerTest : public testing::Test {
protected:
    auto ExtractLines(std::istream& s) const {
        using Line = std::vector<std::string>;
        using Lines = std::vector<Line>;
        Lines lines{};
        std::string buf{};
        while (std::getline(s, buf)) {
            if (buf.find_first_not_of(" \t\r\n") == std::string::npos) continue;
            Line line{};
            for (const auto& ref : std::string_view{ buf } | std::views::split(' ') | std::views::filter([](const auto& v) { return !v.empty(); }))
                line.emplace_back(std::string_view{ ref });
            lines.emplace_back(line);
        }
        return lines;
    }
    void RunTest(std::string_view name) const {
        const std::string testDir = std::filesystem::current_path().string() + '/' + std::string{ TestFolderName } + '/';
        const auto [inputPath, expectedQuadsPath, outputQuadsPath] = std::tuple(
            testDir + std::string{ name } + std::string{ InputSuffix },
            testDir + std::string{ name } + std::string{ ExpectedQuadsSuffix },
            testDir + std::string{ name } + std::string{ OutputQuadsSuffix });
        {
            std::ifstream ifs{ inputPath };
            if (!ifs.is_open() || ExtractLines(ifs).empty()) {
                FAIL() << "File missing or empty: " << inputPath << '\n';
            }
            std::ifstream efs{ expectedQuadsPath };
            if (!efs.is_open()) {
                FAIL() << "File missing: " << expectedQuadsPath << '\n';
            }
            std::ofstream ofs{ outputQuadsPath };
            if (!ofs.is_open()) {
                FAIL() << "File missing: " << outputQuadsPath << '\n';
            }
        }

        {
            Compiler compiler{ std::string_view{ inputPath }, name.contains("ShortCircuit"), std::string_view{ outputQuadsPath } };
            compiler.Compile();
        }

        {
            std::ifstream efs{ expectedQuadsPath };
            std::ifstream ofs{ outputQuadsPath };
            const auto efsLines = ExtractLines(efs);
            const auto ofsLines = ExtractLines(ofs);
            ASSERT_EQ(efsLines.size(), ofsLines.size());
            const auto sz = efsLines.size();
            for (auto i = 0u; i < sz; ++i) {
                const auto& efsLine = efsLines.at(i);
                const auto& ofsLine = ofsLines.at(i);
                // const auto efsCompare = efsLine;
                // const auto ofsCompare = ofsLine;
                const auto efsCompare = std::vector<std::string>{ efsLine.at(0), efsLine.at(1), efsLine.at(2) };
                const auto ofsCompare = std::vector<std::string>{ ofsLine.at(0), ofsLine.at(1), ofsLine.at(2) };
                EXPECT_EQ(efsCompare, ofsCompare);
            }
        }
    }

    void RunTestError(std::string_view name, std::vector<std::string_view> expectedErrorWords) const {
        const std::string testDir = std::filesystem::current_path().string() + '/' + std::string{ TestFolderName } + '/';
        const auto [inputPath] = std::tuple(testDir + std::string{ name } + std::string{ InputSuffix });
        {
            std::ifstream ifs{ inputPath };
            if (!ifs.is_open() || ExtractLines(ifs).empty()) {
                FAIL() << "File missing or empty: " << inputPath << '\n';
            }
            ASSERT_FALSE(expectedErrorWords.empty());
        }
        std::streambuf* coutBuf = std::cout.rdbuf();
        std::stringstream coutRedirect;
        std::cout.rdbuf(coutRedirect.rdbuf());
        {
            Compiler compiler{ std::string_view{ inputPath }, name.contains("ShortCircuit") };
            compiler.Compile();
        }
        std::string coutContent = coutRedirect.str();
        std::cout.rdbuf(coutBuf);
        {
            for (const auto& word : expectedErrorWords)
                ASSERT_PRED1([&coutContent](auto w) { return coutContent.contains(w); }, word);
        }
    }
};

#define DEFINE_TEST(TestName) \
    TEST_F(CompilerTest, TestName) { RunTest(this->test_info_->name()); }

#define DEFINE_TEST_ERROR(TestName, ...) \
    TEST_F(CompilerTest, TestName) { RunTestError(this->test_info_->name(), { __VA_ARGS__ }); }

DEFINE_TEST(User00FunctionDefinition)
DEFINE_TEST(User01TableItem)
DEFINE_TEST(User02Assignment)
DEFINE_TEST(User03FunctionCall)
DEFINE_TEST(User04MethodCall)
DEFINE_TEST(User05TableConstruction)
DEFINE_TEST(User06Unary)
DEFINE_TEST(User07Arithmetic)
DEFINE_TEST(User08Relational)
DEFINE_TEST(User09Boolean)
DEFINE_TEST(User10Branch)
DEFINE_TEST(User11Loop)
DEFINE_TEST(User12ReturnBreakContinue)
DEFINE_TEST(User13ShortCircuit)
DEFINE_TEST(User14TempRecycle)

DEFINE_TEST(FAQ19)
DEFINE_TEST(FAQ20)
DEFINE_TEST(FAQ21)
DEFINE_TEST(FAQ22)
DEFINE_TEST(FAQ23)
DEFINE_TEST_ERROR(FAQ28, "arithmetic")
DEFINE_TEST_ERROR(FAQ28ShortCircuit, "arithmetic")
DEFINE_TEST(FAQ29)

DEFINE_TEST(Front4Slide23)
DEFINE_TEST(Front4Slide27)
DEFINE_TEST(Front4Slide28)
DEFINE_TEST(Front4Slide29)
DEFINE_TEST(Front4Slide30)
DEFINE_TEST(Front4Slide31)
DEFINE_TEST(Front4Slide34)
DEFINE_TEST(Front4Slide35)
DEFINE_TEST(Front4Slide39)
DEFINE_TEST(Front4Slide40)
DEFINE_TEST(Front4Slide35ShortCircuit)
DEFINE_TEST(Front4Slide39ShortCircuit)
DEFINE_TEST(Front4Slide40ShortCircuit)

DEFINE_TEST(Lecture9Slide41)
DEFINE_TEST(Lecture9Slide41ShortCircuit)
DEFINE_TEST(Lecture10Slide23)
DEFINE_TEST(Lecture10Slide24)
DEFINE_TEST(Lecture10Slide27)
DEFINE_TEST(Lecture10Slide28)
DEFINE_TEST(Lecture10Slide29)
DEFINE_TEST(Lecture10Slide30)
DEFINE_TEST(Lecture10Slide31)
DEFINE_TEST(Lecture10Slide33)
DEFINE_TEST(Lecture10Slide34)
DEFINE_TEST(Lecture10Slide37)
DEFINE_TEST(Lecture11Slide5)
DEFINE_TEST(Lecture11Slide6)
DEFINE_TEST(Lecture11Slide6ShortCircuit)
DEFINE_TEST(Lecture11Slide7)
DEFINE_TEST(Lecture11Slide7ShortCircuit)
DEFINE_TEST(Lecture11Slide25)

DEFINE_TEST(Exam00FuncDecl)
DEFINE_TEST(Exam01Calls)
DEFINE_TEST(Exam02AssignmentsSimple)
DEFINE_TEST(Exam03AssignmentsComplex)
DEFINE_TEST(Exam04AssignmentsObjects)
DEFINE_TEST(Exam05BasicExpr)
DEFINE_TEST(Exam06ObjectCreationExpr)
DEFINE_TEST(Exam07VarMaths)
DEFINE_TEST(Exam08ConstMaths)
DEFINE_TEST(Exam09Relational)
DEFINE_TEST(Exam10IfElse)
DEFINE_TEST(Exam11Backpatch0)
DEFINE_TEST(Exam12Backpatch1)
DEFINE_TEST(Exam13Backpatch2)
DEFINE_TEST(Exam14Backpatch3)
DEFINE_TEST(Exam15FlowControl)
DEFINE_TEST_ERROR(Exam16FlowControlError, "continue", "break", "return")
DEFINE_TEST(Exam17Vavouris)

DEFINE_TEST(Exam18Backpatch0ShortCircuit)
DEFINE_TEST(Exam19Backpatch1ShortCircuit)
DEFINE_TEST(Exam20Backpatch2ShortCircuit)
DEFINE_TEST(Exam21Backpatch3ShortCircuit)
DEFINE_TEST(Exam22VavourisShortCircuit)
DEFINE_TEST(Exam23RelationalShortCircuit)

DEFINE_TEST_ERROR(Error00, "}")
DEFINE_TEST_ERROR(Error01, "if")
DEFINE_TEST_ERROR(Error03, "}")
DEFINE_TEST_ERROR(Error04, "constant")
DEFINE_TEST_ERROR(Error05, "true", "1", "2", "str", "print")
DEFINE_TEST_ERROR(Error06, "enclosing")
DEFINE_TEST_ERROR(Error07, "shadows", "undefined", "redefinition")

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    // ::testing::GTEST_FLAG(break_on_failure) = true;
    // ::testing::GTEST_FLAG(filter) = "CompilerTest.Error05";
    return RUN_ALL_TESTS();
}
