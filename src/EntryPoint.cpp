#include "Compiler.h"

#include "Context.h"
#include "ParserContext.h"
#include "ScannerContext.h"

#include <filesystem>
#include <iostream>
#include <span>
#include <string>

namespace {
    constexpr bool ShortCircuitEvaluationEnabled = true;
}

auto main(int argc, char** argv) -> int {
    if (!(argc >= 2)) {
        std::cout << "Parameters must be at least one, given: " << argc - 1 << '\n';
        std::exit(EXIT_FAILURE);
    }
    const auto argvSpan = std::span{ argv, static_cast<size_t>(argc) };
    const auto inputPath = std::filesystem::current_path().string() + "/" + argvSpan[1];

    Compiler compiler{ std::string_view{ inputPath }, ShortCircuitEvaluationEnabled };
    compiler.Compile();
}
