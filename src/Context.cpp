#include "Context.h"

#include "ParserContext.h"

#include "ParserUtilities.h"
#include "QuadHolder.h"
#include "ScannerContext.h"
#include "ScannerUtilities.h"
#include "Symbol.h"
#include "SymbolTable.h"

Context::Context(bool shortCircuitEvaluationEnabled_) : scannerContext{ std::make_unique<ScannerContext>() },
                                                        parserContext{ std::make_unique<ParserContext>(shortCircuitEvaluationEnabled_) } {}
auto Context::ScannerCtx() -> ScannerContext* { return scannerContext.get(); }
auto Context::ParserCtx() -> ParserContext* { return parserContext.get(); }
