#include "Symbol.h"

#include <iomanip>
#include <ostream>
#include <sstream>

#include <cassert>

namespace {
    static auto TypeToString(SymbolType type) -> std::string {
        switch (type) {
            case SymbolType::TempVar: return "Temporary Variable";
            case SymbolType::GlobalVar: return "Global Variable";
            case SymbolType::LocalVar: return "Local Variable";
            case SymbolType::FormalArg: return "Formal Argument";
            case SymbolType::UserFunction: return "User Function";
            case SymbolType::LibraryFunction: return "Library Function";
            default: assert(false); return {};
        }
    }
}    // namespace

auto operator<<(std::ostream& os, const FormalArgList& obj) -> std::ostream& {
    os << '(';
    const auto sz = std::ranges::size(obj);
    for (size_t i = 0; i < sz; ++i) {
        const auto argName = obj.at(i)->name;
        os << std::quoted(argName);
        if (i < sz - 1) os << ", ";
    }
    os << ')';
    return os;
}

auto Symbol::TypeToString() const -> std::string { return ::TypeToString(type); }

auto operator<<(std::ostream& os, const Symbol& symbol) -> std::ostream& {
    std::stringstream formalArgListSS{};
    if (symbol.type == SymbolType::UserFunction) formalArgListSS << ' ' << symbol.funcDefInfo.formalArgList;
    constexpr auto NameWidth = 40;
    constexpr auto TypeWidth = 25;
    constexpr auto LocationWidth = 20;
    constexpr auto ScopeWidth = 10;
    constexpr auto CellFill = ' ';
    std::stringstream ss{};
    ss << std::quoted(symbol.name);
    const auto symbolName = ss.str() + formalArgListSS.str();
    ss.str("");
    ss << '[' << TypeToString(symbol.type) << ']';
    const auto symbolType = ss.str();
    ss.str("");
    ss << "Location" << '(' << symbol.location << ')';
    const auto symbolLocation = ss.str();
    ss.str("");
    ss << "Scope" << '(' << symbol.scope << ')';
    const auto symbolScope = ss.str();
    ss.str("");
    os << std::left << std::setw(NameWidth) << std::setfill(CellFill) << symbolName
       << std::left << std::setw(TypeWidth) << std::setfill(CellFill) << symbolType
       << std::left << std::setw(LocationWidth) << std::setfill(CellFill) << symbolLocation
       << std::left << std::setw(ScopeWidth) << std::setfill(CellFill) << symbolScope;
    return os;
}
