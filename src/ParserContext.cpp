#include "ParserContext.h"

#include "Expression.h"
#include "ParserUtilities.h"
#include "QuadHolder.h"
#include "Symbol.h"
#include "SymbolTable.h"

#include <algorithm>
#include <cassert>
#include <cfenv>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <memory>
#include <ranges>
#include <sstream>

ParserContext::ParserContext(bool ShortCircuitEvaluationEnabled_) : shortCircuitEvaluationEnabled{ ShortCircuitEvaluationEnabled_ },
                                                                    symbolTable{ std::make_unique<SymbolTable>() },
                                                                    quadHolder{ std::make_unique<QuadHolder>() },
                                                                    scopeStack{ std::make_unique<ScopeStackType>() },
                                                                    scopeSpaceStack{ std::make_unique<ScopeSpaceStackType>() },
                                                                    hiddenSymbolGenerator{ std::make_unique<HiddenSymbolGeneratorType>(symbolTable.get(), scopeStack.get(), scopeSpaceStack.get()) } {}

auto ParserContext::GetSymbolTable() const -> SymbolTable* { return symbolTable.get(); }
auto ParserContext::GetQuadHolder() const -> QuadHolder* { return quadHolder.get(); }

auto ParserContext::SemanticError(const Location& location, const std::string& name, const std::string& message) -> void {
    std::cout << "Semantic Error: " << location << ' ' << std::quoted(name) << ' ' << message << '\n';
    if (!parseErrorFlag) parseErrorFlag = true;
}

auto ParserContext::CheckFunctionTypeModificationError(const Expression* expression, const Location& loc) -> void {
    assert(expression->symbol);
    auto symbol = expression->symbol;
    std::stringstream ss{};
    ss << "Symbol modification of constant type [" << symbol->TypeToString() << "]";
    switch (symbol->type) {
        case SymbolType::UserFunction:
        case SymbolType::LibraryFunction:
            SemanticError(loc, symbol->name, ss.str());
            return;
        case SymbolType::TempVar:
        case SymbolType::GlobalVar:
        case SymbolType::LocalVar:
        case SymbolType::FormalArg: return;
        default: assert(false); return;
    }
}
auto ParserContext::CheckValidArithmetic(const Expression* lvalue, const Location& loc) -> void {
    if (!lvalue->IsValidArithmeticArgument()) {
        std::stringstream ss{};
        ss << *lvalue;
        SemanticError(loc, ss.str(), "lvalue is not a valid arithmetic argument");
    }
}

namespace {
    /* File Scope Symbols */

    auto OpCodeFromString(const std::string& val) -> OpCode {
        if (val == "Increment") return OpCode::Add;
        if (val == "Decrement") return OpCode::Subtract;
        if (val == "Add") return OpCode::Add;
        if (val == "Subtract") return OpCode::Subtract;
        if (val == "Multiply") return OpCode::Multiply;
        if (val == "Divide") return OpCode::Divide;
        if (val == "Modulo") return OpCode::Modulo;
        if (val == "And") return OpCode::And;
        if (val == "Or") return OpCode::Or;
        if (val == "Equal") return OpCode::IfEqual;
        if (val == "NotEqual") return OpCode::IfNotEqual;
        if (val == "Less") return OpCode::IfLess;
        if (val == "LessEqual") return OpCode::IfLessEqual;
        if (val == "Greater") return OpCode::IfGreater;
        if (val == "GreaterEqual") return OpCode::IfGreaterEqual;
        assert(false);
    }

    auto GetArithmeticResult(double leftNumber, OpCode opCode, double rightNumber) -> double {
        double result{};
        switch (opCode) {
            case OpCode::Add: result = leftNumber + rightNumber; break;
            case OpCode::Subtract: result = leftNumber - rightNumber; break;
            case OpCode::Multiply: result = leftNumber * rightNumber; break;
            case OpCode::Divide: result = leftNumber / rightNumber; break;
            case OpCode::Modulo: result = std::remainder(leftNumber, rightNumber); break;
            default: assert(false);
        }
        if (std::fetestexcept(FE_DIVBYZERO) || std::fetestexcept(FE_INVALID)) {
            std::feclearexcept(FE_ALL_EXCEPT);
            assert(false);
        }
        return result;
    }

    auto GetBooleanResult(bool leftArg, OpCode opCode, bool rightArg) -> bool {
        switch (opCode) {
            case OpCode::And: return leftArg && rightArg;
            case OpCode::Or: return leftArg || rightArg;
            default: assert(false);
        }
    }
    auto GetBooleanResult(OpCode opCode, bool arg) -> bool {
        assert(opCode == OpCode::Not);
        return !arg;
    }

    ExpressionType ExpressionTypeFromSymbolType(SymbolType symbolType) {
        switch (symbolType) {
            case SymbolType::TempVar:
            case SymbolType::GlobalVar:
            case SymbolType::FormalArg:
            case SymbolType::LocalVar: return ExpressionType::Variable;
            case SymbolType::UserFunction: return ExpressionType::UserFunction;
            case SymbolType::LibraryFunction: return ExpressionType::LibraryFunction;
            default: assert(false); return {};
        }
    }

    constexpr auto GetBreakError() { return "\"break\" not present in loop body"; }
    constexpr auto GetContinueError() { return "\"continue\" not present in loop body"; }
    constexpr auto GetReturnError() { return "\"return\" not present in function body"; }

}    // namespace

/* Interface Implementation */

auto ParserContext::HandleScopeBegin(const std::string& scopeName) -> void {
    scopeStack->Push(scopeName);
    symbolTable->CheckScopeUpperLimit(scopeStack->GetScopeNum());
}
auto ParserContext::HandleScopeEnd() -> void {
    symbolTable->Hide(scopeStack->GetScopeNum());
    scopeStack->Pop();
}
auto ParserContext::HandleScopeSpaceBegin(ScopeSpaceName scopeSpaceName) -> void {
    scopeSpaceStack->Push({ scopeSpaceName, 0UL });
}
auto ParserContext::HandleScopeSpaceEnd() -> void {
    scopeSpaceStack->Pop();
}

auto ParserContext::Handle_Program() -> bool { return parseErrorFlag; }
auto ParserContext::Handle_StatementList_Empty() -> StatementSPtr {
    return Statement::Make();
}
auto ParserContext::HandleError() -> StatementSPtr {
    return Statement::Make();
}
auto ParserContext::Handle_StatementList_NextStatement(const StatementSPtr& mergedStatement, const StatementSPtr& statement) -> StatementSPtr {
    return Statement::Merge(mergedStatement, statement);
}

auto ParserContext::Handle_BlockBegin() -> void {
    HandleScopeBegin(ScopeStackType::BlockScopeName);
}
auto ParserContext::Handle_BlockEnd() -> void { HandleScopeEnd(); }
auto ParserContext::Handle_Statement_NonBlockStatement(const StatementSPtr& statement) -> StatementSPtr {
    return statement;
}
auto ParserContext::Handle_Statement_StatementList(const StatementSPtr& statement) -> StatementSPtr {
    return statement;
}
auto ParserContext::Handle_NonBlockStatement_Expression() -> StatementSPtr {
    hiddenSymbolGenerator->ResetTemporaryVariableNames();
    return Statement::Make();
}
auto ParserContext::Handle_NonBlockStatement_IfStatement(const StatementSPtr& statement) -> StatementSPtr {
    hiddenSymbolGenerator->ResetTemporaryVariableNames();
    return statement;
}
auto ParserContext::Handle_NonBlockStatement_WhileStatement() -> StatementSPtr {
    hiddenSymbolGenerator->ResetTemporaryVariableNames();
    return Statement::Make();
}
auto ParserContext::Handle_NonBlockStatement_ForStatement() -> StatementSPtr {
    hiddenSymbolGenerator->ResetTemporaryVariableNames();
    return Statement::Make();
}
auto ParserContext::Handle_NonBlockStatement_ReturnStatement() -> StatementSPtr {
    hiddenSymbolGenerator->ResetTemporaryVariableNames();
    return Statement::Make();
}
auto ParserContext::Handle_NonBlockStatement_Break(const Location& loc) -> StatementSPtr {
    if (!scopeStack->IsScopeLoopEnclosed()) SemanticError(loc, {}, GetBreakError());
    const StatementSPtr statement = Statement::MakeBreak({ quadHolder->GetNextLabelPlus(0) });
    quadHolder->EmitIncompleteJump(loc);
    return statement;
}
auto ParserContext::Handle_NonBlockStatement_Continue(const Location& loc) -> StatementSPtr {
    if (!scopeStack->IsScopeLoopEnclosed()) SemanticError(loc, {}, GetContinueError());
    const StatementSPtr statement = Statement::MakeContinue({ quadHolder->GetNextLabelPlus(0) });
    quadHolder->EmitIncompleteJump(loc);
    return statement;
}
auto ParserContext::Handle_NonBlockStatement_FunctionDefinition() -> StatementSPtr {
    return Statement::Make();
}
auto ParserContext::Handle_NonBlockStatement_Empty() -> StatementSPtr {
    return Statement::Make();
}
auto ParserContext::Handle_NonBlockStatement_Error() -> StatementSPtr {
    hiddenSymbolGenerator->ResetTemporaryVariableNames();
    return Statement::Make();
}

auto ParserContext::Handle_Expression_AssignExpression(const ExpressionSPtr& expression) -> ExpressionSPtr { return expression; }
auto ParserContext::Handle_Expression_ShortCircuitEvaluate(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    if (!shortCircuitEvaluationEnabled) return expression;
    return ShortCircuitEvaluate(expression, loc);
}

auto ParserContext::Handle_Expression_OperationExpressionNotShortCircuitEvaluated(const ExpressionSPtr& expression) -> ExpressionSPtr { return expression; }
auto ParserContext::Handle_Expression_Term(const ExpressionSPtr& expression) -> ExpressionSPtr { return expression; }

auto ParserContext::Handle_Arithmetic_Operation(const ExpressionSPtr& expressionLeft, const std::string& op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    CheckValidArithmetic(expressionLeft.get(), locLeft);
    CheckValidArithmetic(expressionRight.get(), locRight);
    const OpCode binaryOpCode = OpCodeFromString(op);
    ExpressionSPtr result{};
    if (expressionLeft->GetType() == ExpressionType::ConstNumber && expressionRight->GetType() == ExpressionType::ConstNumber)
        return Expression::MakeConstNumber(GetArithmeticResult(expressionLeft->GetConstNumber(), binaryOpCode, expressionRight->GetConstNumber()));
    result = Expression::Make(ExpressionType::Arithmetic, hiddenSymbolGenerator->RecycleOrGetTemporaryVariable({ expressionLeft->GetSymbol(), expressionRight->GetSymbol() }, locLeft));
    quadHolder->Emit(binaryOpCode, result, expressionLeft, expressionRight, locLeft);
    return result;
}

auto ParserContext::Equality_Relational_Impl_Default(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    (void)locRight;
    ExpressionSPtr result = Expression::Make(ExpressionType::Boolean, hiddenSymbolGenerator->RecycleOrGetTemporaryVariable({ expressionLeft->GetSymbol(), expressionRight->GetSymbol() }, locLeft));
    quadHolder->EmitConditionalJump(op, expressionLeft, expressionRight, quadHolder->GetNextLabelPlus(3), locLeft);
    quadHolder->Emit(OpCode::Assign, result, Expression::MakeConstBoolean(false), nullptr, locLeft);
    quadHolder->EmitJump(quadHolder->GetNextLabelPlus(2), locLeft);
    quadHolder->Emit(OpCode::Assign, result, Expression::MakeConstBoolean(true), nullptr, locLeft);
    return result;
}
auto ParserContext::ShortCircuitEvaluate(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    assert(shortCircuitEvaluationEnabled);
    if (expression->GetType() != ExpressionType::Boolean) return expression;
    const Label label = quadHolder->GetNextLabelPlus(0);
    quadHolder->PatchJumps(expression->GetShortCircuitLists().trueList, label);
    quadHolder->PatchJumps(expression->GetShortCircuitLists().falseList, label + 2);
    assert(expression->GetSymbol() == nullptr);
    ExpressionSPtr evaluatedExpression = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::Assign, evaluatedExpression, Expression::MakeConstBoolean(true), nullptr, loc);
    quadHolder->EmitJump(label + 3, loc);
    quadHolder->Emit(OpCode::Assign, evaluatedExpression, Expression::MakeConstBoolean(false), nullptr, loc);
    return evaluatedExpression;
}
auto ParserContext::Equality_Relational_Impl_ShortCircuit(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight_, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    ExpressionSPtr expressionRight = ShortCircuitEvaluate(expressionRight_, locRight);
    const Label label = quadHolder->GetNextLabelPlus(0);
    ExpressionSPtr result = Expression::MakeShortCircuit(ShortCircuitLists{ { label }, { label + 1 } });
    quadHolder->EmitIncompleteConditionalJump(op, expressionLeft, expressionRight, locLeft);
    quadHolder->EmitIncompleteJump(locLeft);
    return result;
}
auto ParserContext::Equality_Relational_Impl(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    if (shortCircuitEvaluationEnabled)
        return Equality_Relational_Impl_ShortCircuit(expressionLeft, op, expressionRight, locLeft, locRight);
    return Equality_Relational_Impl_Default(expressionLeft, op, expressionRight, locLeft, locRight);
}
auto ParserContext::Handle_Equality_Operation_Prefix(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    if (expression->IsConstBooleanConvertible()) return expression;
    if (shortCircuitEvaluationEnabled) return ShortCircuitEvaluate(expression, loc);
    return expression;
}
auto ParserContext::Handle_Equality_Operation(const ExpressionSPtr& expressionLeft, const std::string& opString, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    (void)locRight;
    const OpCode op = OpCodeFromString(opString);
    const auto compareFunc = [](const auto& left, OpCode eqOp, const auto& right) -> bool { switch(eqOp) {
            case OpCode::IfEqual: return !(left < right && right < left);
            case OpCode::IfNotEqual: return !!(left < right && right < left);
            default:assert(false); return{}; }; };
    const auto leftType = expressionLeft->GetType();
    const auto rightType = expressionRight->GetType();
    if ((leftType == ExpressionType::TableItem && rightType == ExpressionType::Nil) ||
        (leftType == ExpressionType::Nil && rightType == ExpressionType::TableItem))
        return Expression::MakeConstBoolean(compareFunc(true, op, false));
    if (leftType == ExpressionType::ConstNumber && rightType == ExpressionType::ConstNumber)
        return Expression::MakeConstBoolean(compareFunc(expressionLeft->GetConstNumber(), op, expressionRight->GetConstNumber()));
    if (leftType == ExpressionType::ConstBoolean && rightType == ExpressionType::ConstBoolean)
        return Expression::MakeConstBoolean(compareFunc(expressionLeft->GetConstBoolean(), op, expressionRight->GetConstBoolean()));
    if (leftType == ExpressionType::ConstString && rightType == ExpressionType::ConstString)
        return Expression::MakeConstBoolean(compareFunc(expressionLeft->GetConstString(), op, expressionRight->GetConstString()));
    if (leftType != ExpressionType::Variable &&
        rightType != ExpressionType::Variable &&
        leftType != rightType) {
        std::stringstream ss{};
        ss << *expressionLeft << " " << opString << " " << *expressionRight;
        SemanticError(locLeft, ss.str(), "not equality comparable types");
    }
    return Equality_Relational_Impl(expressionLeft, op, expressionRight, locLeft, locRight);
}
auto ParserContext::Handle_Relational_Operation(const ExpressionSPtr& expressionLeft, const std::string& opString, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    CheckValidArithmetic(expressionLeft.get(), locLeft);
    CheckValidArithmetic(expressionRight.get(), locRight);
    const OpCode op = OpCodeFromString(opString);
    const auto compareFunc = [](const auto& left, OpCode relOp, const auto& right) -> bool { switch(relOp) {
            case OpCode::IfLess: return left < right;
            case OpCode::IfLessEqual: return left <= right;
            case OpCode::IfGreater: return left > right;
            case OpCode::IfGreaterEqual: return left >= right;
            default:assert(false); return{}; }; };
    if (expressionLeft->GetType() == ExpressionType::ConstNumber && expressionRight->GetType() == ExpressionType::ConstNumber)
        return Expression::MakeConstBoolean(compareFunc(expressionLeft->GetConstNumber(), op, expressionRight->GetConstNumber()));
    return Equality_Relational_Impl(expressionLeft, op, expressionRight, locLeft, locRight);
}

auto ParserContext::BooleanOperation_Impl_Default(const ExpressionSPtr& expressionLeft, OpCode op, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    (void)locRight;
    ExpressionSPtr result = Expression::Make(ExpressionType::Boolean, hiddenSymbolGenerator->RecycleOrGetTemporaryVariable({ expressionLeft->GetSymbol(), expressionRight->GetSymbol() }, locLeft));
    quadHolder->Emit(op, result, expressionLeft, expressionRight, locLeft);
    return result;
}

auto ParserContext::ShortCircuitTruthTest(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    if (expression->GetType() == ExpressionType::Boolean)
        return Expression::MakeShortCircuit(ShortCircuitLists{ expression->GetShortCircuitLists().trueList, expression->GetShortCircuitLists().falseList });
    const Label label = quadHolder->GetNextLabelPlus(0);
    ExpressionSPtr result = Expression::MakeShortCircuit(ShortCircuitLists{ { label }, { label + 1 } });
    quadHolder->EmitIncompleteConditionalJump(OpCode::IfEqual, expression, Expression::MakeConstBoolean(true), loc);
    quadHolder->EmitIncompleteJump(loc);
    return result;
}
auto ParserContext::Or_ShortCircuit(const ExpressionSPtr& shortCircuitExpressionLeft, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft) -> ExpressionSPtr {
    ExpressionSPtr shortCircuitExpressionRight = ShortCircuitTruthTest(expressionRight, locLeft);
    quadHolder->PatchJumps(shortCircuitExpressionLeft->GetShortCircuitLists().falseList, jumpDest);
    return Expression::MakeShortCircuit(ShortCircuitLists{ shortCircuitExpressionLeft->GetShortCircuitLists().trueList + shortCircuitExpressionRight->GetShortCircuitLists().trueList,
                                                           shortCircuitExpressionRight->GetShortCircuitLists().falseList });
}
auto ParserContext::And_ShortCircuit(const ExpressionSPtr& shortCircuitExpressionLeft, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft) -> ExpressionSPtr {
    ExpressionSPtr shortCircuitExpressionRight = ShortCircuitTruthTest(expressionRight, locLeft);
    quadHolder->PatchJumps(shortCircuitExpressionLeft->GetShortCircuitLists().trueList, jumpDest);

    return Expression::MakeShortCircuit(ShortCircuitLists{
        shortCircuitExpressionRight->GetShortCircuitLists().trueList,
        shortCircuitExpressionLeft->GetShortCircuitLists().falseList + shortCircuitExpressionRight->GetShortCircuitLists().falseList,
    });
}
auto ParserContext::BooleanOperation_Impl_ShortCircuit(const ExpressionSPtr& expressionLeft, OpCode op, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    (void)locRight;
    switch (op) {
        case OpCode::Or: return Or_ShortCircuit(expressionLeft, jumpDest, expressionRight, locLeft);
        case OpCode::And: return And_ShortCircuit(expressionLeft, jumpDest, expressionRight, locLeft);
        default: assert(false); return {};
    }
}
auto ParserContext::BooleanOperation_Impl(const ExpressionSPtr& expressionLeft, OpCode op, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    if (shortCircuitEvaluationEnabled)
        return BooleanOperation_Impl_ShortCircuit(expressionLeft, op, jumpDest, expressionRight, locLeft, locRight);
    return BooleanOperation_Impl_Default(expressionLeft, op, expressionRight, locLeft, locRight);
}
auto ParserContext::Handle_Boolean_Operation_Prefix(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    if (expression->IsConstBooleanConvertible()) return expression;
    if (shortCircuitEvaluationEnabled) return ShortCircuitTruthTest(expression, loc);
    return expression;
}
auto ParserContext::Handle_Boolean_Operation(const ExpressionSPtr& expressionLeft, const std::string& opString, Label jumpDest, const ExpressionSPtr& expressionRight, const Location& locLeft, const Location& locRight) -> ExpressionSPtr {
    (void)locRight;
    const OpCode op = OpCodeFromString(opString);
    if (expressionLeft->IsConstBooleanConvertible() && expressionRight->IsConstBooleanConvertible())
        return Expression::MakeConstBoolean(GetBooleanResult(expressionLeft->GetConstBooleanConversion(), op, expressionRight->GetConstBooleanConversion()));
    return BooleanOperation_Impl(expressionLeft, op, jumpDest, expressionRight, locLeft, locRight);
}

auto ParserContext::Handle_Term_Expression(const ExpressionSPtr& expression) -> ExpressionSPtr { return expression; }
auto ParserContext::Handle_Term_UnaryMinus_ExpressionNotShortiCircuitEvaluated(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    CheckValidArithmetic(expression.get(), loc);
    if (expression->GetType() == ExpressionType::ConstNumber)
        return Expression::MakeConstNumber((-1.00) * expression->GetConstNumber());
    ExpressionSPtr result = Expression::Make(ExpressionType::Arithmetic, hiddenSymbolGenerator->RecycleOrGetTemporaryVariable({ expression->GetSymbol() }, loc));
    quadHolder->Emit(OpCode::Multiply, result, Expression::MakeConstNumber(-1.00), expression, loc);
    return result;
}

auto ParserContext::Term_Not_Expression_Impl_Default(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    ExpressionSPtr result = Expression::Make(ExpressionType::Boolean, hiddenSymbolGenerator->RecycleOrGetTemporaryVariable({ expression->GetSymbol() }, loc));
    quadHolder->Emit(OpCode::Not, result, expression, nullptr, loc);
    return result;
}
auto ParserContext::Term_Not_Expression_Impl_ShortCircuit(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    ExpressionSPtr shortCircuitExpression = ShortCircuitTruthTest(expression, loc);
    return Expression::MakeShortCircuit(ShortCircuitLists{ shortCircuitExpression->GetShortCircuitLists().falseList, shortCircuitExpression->GetShortCircuitLists().trueList });
}
auto ParserContext::Term_Not_Expression_Impl(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    if (shortCircuitEvaluationEnabled)
        return Term_Not_Expression_Impl_ShortCircuit(expression, loc);
    return Term_Not_Expression_Impl_Default(expression, loc);
}
auto ParserContext::Handle_Term_Not_ExpressionNotShortCircuitEvaluated(const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    if (expression->IsConstBooleanConvertible())
        return Expression::MakeConstBoolean(GetBooleanResult(OpCode::Not, expression->GetConstBooleanConversion()));
    return Term_Not_Expression_Impl(expression, loc);
}

auto ParserContext::Handle_Term_UnaryOp_Lvalue(const std::string& op, const ExpressionSPtr& lvalue, const Location& loc) -> ExpressionSPtr {
    CheckFunctionTypeModificationError(lvalue.get(), loc);
    CheckValidArithmetic(lvalue.get(), loc);
    const OpCode unaryOpCode = OpCodeFromString(op);
    if (lvalue->GetType() == ExpressionType::TableItem) {
        ExpressionSPtr result = Expression::Make(ExpressionType::Arithmetic, hiddenSymbolGenerator->GetTemporaryVariable(loc));
        quadHolder->Emit(OpCode::TableGetElement, result, lvalue, lvalue->GetIndex(), loc);
        quadHolder->Emit(unaryOpCode, result, result, Expression::MakeConstNumber(1), loc);
        quadHolder->Emit(OpCode::TableSetElement, result, lvalue, lvalue->GetIndex(), loc);
        return result;
    }
    quadHolder->Emit(unaryOpCode, lvalue, lvalue, Expression::MakeConstNumber(1), loc);
    ExpressionSPtr result = Expression::Make(ExpressionType::Arithmetic, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::Assign, result, lvalue, nullptr, loc);
    return result;
}
auto ParserContext::Handle_Term_Lvalue_UnaryOp(const ExpressionSPtr& lvalue, const std::string& op, const Location& loc) -> ExpressionSPtr {
    CheckFunctionTypeModificationError(lvalue.get(), loc);
    CheckValidArithmetic(lvalue.get(), loc);
    const OpCode unaryOpCode = OpCodeFromString(op);
    if (lvalue->GetType() == ExpressionType::TableItem) {
        ExpressionSPtr lvalueNew = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
        quadHolder->Emit(OpCode::TableGetElement, lvalueNew, lvalue, lvalue->GetIndex(), loc);
        ExpressionSPtr result = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
        quadHolder->Emit(OpCode::Assign, result, lvalueNew, nullptr, loc);
        quadHolder->Emit(unaryOpCode, lvalueNew, lvalueNew, Expression::MakeConstNumber(1), loc);
        quadHolder->Emit(OpCode::TableSetElement, lvalueNew, lvalue, lvalue->GetIndex(), loc);
        return result;
    }
    ExpressionSPtr result = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::Assign, result, lvalue, nullptr, loc);
    quadHolder->Emit(unaryOpCode, lvalue, lvalue, Expression::MakeConstNumber(1), loc);
    return result;
}
auto ParserContext::Handle_Term_Primary(const ExpressionSPtr& expression) -> ExpressionSPtr { return expression; }

auto ParserContext::Handle_AssignExpression(const ExpressionSPtr& lvalue, const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    CheckFunctionTypeModificationError(lvalue.get(), loc);

    if (lvalue->GetType() == ExpressionType::TableItem) {
        quadHolder->Emit(OpCode::TableSetElement, expression, lvalue, lvalue->GetIndex(), loc);
        if (expression->IsConst()) return expression;
        ExpressionSPtr assignExpression = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
        quadHolder->Emit(OpCode::TableGetElement, assignExpression, lvalue, lvalue->GetIndex(), loc);
        return assignExpression;
    }
    quadHolder->Emit(OpCode::Assign, lvalue, expression, nullptr, loc);
    if (expression->IsConst()) return expression;
    ExpressionSPtr assignExpression = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::Assign, assignExpression, lvalue, nullptr, loc);
    return assignExpression;
}

auto ParserContext::Handle_Primary_Lvalue(const ExpressionSPtr& lvalue, const Location& loc) -> ExpressionSPtr {
    if (lvalue->GetType() != ExpressionType::TableItem) return lvalue;
    ExpressionSPtr lvalueNew = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::TableGetElement, lvalueNew, lvalue, lvalue->GetIndex(), loc);
    return lvalueNew;
}
auto ParserContext::Handle_Primary_Call(const ExpressionSPtr& call) -> ExpressionSPtr {
    return call;
}
auto ParserContext::Handle_Primary_TableDefinition(const ExpressionSPtr& table) -> ExpressionSPtr {
    return table;
}
auto ParserContext::Handle_Primary_FunctionDefinition(const Symbol* funcDef) -> ExpressionSPtr {
    assert(funcDef->type == SymbolType::UserFunction);
    return Expression::Make(ExpressionType::UserFunction, funcDef);
}
auto ParserContext::Handle_Primary_Constant(const ExpressionSPtr& constant) -> ExpressionSPtr {
    return constant;
}

auto ParserContext::Handle_Lvalue_Identifier(const std::string& id, const Location& loc) -> ExpressionSPtr {
    const Symbol* existingSymbol = symbolTable->LookUpAcrossScopes(id, scopeStack->GetScopeNum());
    if (existingSymbol == nullptr) {
        const auto type = (scopeStack->IsScopeGlobal()) ? SymbolType::GlobalVar : SymbolType::LocalVar;
        const Symbol* insertedSymbol = symbolTable->Insert({ type, id, scopeStack->GetScopeNum(), scopeSpaceStack->GetScopeSpace(), loc });
        scopeSpaceStack->IncrementOffset();
        existingSymbol = insertedSymbol;
    } else {
        const auto ResolveAccessRights = [this](const Symbol* const symbol) -> bool {
            switch (symbol->type) {
                case SymbolType::GlobalVar:
                case SymbolType::UserFunction:
                case SymbolType::LibraryFunction: return true;
                case SymbolType::LocalVar:
                case SymbolType::FormalArg: return !scopeStack->IsSymbolOutsideEnclosingFunction(symbol->scope);
                default: assert(false); return {};
            }
        };
        const bool hasAccess = ResolveAccessRights(existingSymbol);
        if (!hasAccess) {
            SemanticError(loc, id, "refers to symbol outside of the enclosing function scope");
            existingSymbol = hiddenSymbolGenerator->GetTemporaryVariable(loc);
        }
    }
    return Expression::Make(ExpressionTypeFromSymbolType(existingSymbol->type), existingSymbol);
}
auto ParserContext::Handle_Lvalue_Local_Identifier(const std::string& id, const Location& loc) -> ExpressionSPtr {
    const Symbol* existingSymbol = symbolTable->LookUpAtScope(id, scopeStack->GetScopeNum());
    if (existingSymbol == nullptr && symbolTable->IsLibraryFunction(id)) {
        std::stringstream ss{};
        ss << "shadows [Library Function] \"" << id << "\"";
        SemanticError(loc, id, ss.str());
        existingSymbol = hiddenSymbolGenerator->GetTemporaryVariable(loc);
    } else if (existingSymbol == nullptr) {
        const auto type = (scopeStack->GetScopeNum() == SymbolTable::GlobalScope) ? SymbolType::GlobalVar : SymbolType::LocalVar;
        const Symbol* insertedSymbol = symbolTable->Insert({ type, id, scopeStack->GetScopeNum(), scopeSpaceStack->GetScopeSpace(), loc });
        scopeSpaceStack->IncrementOffset();
        existingSymbol = insertedSymbol;
    }
    return Expression::Make(ExpressionTypeFromSymbolType(existingSymbol->type), existingSymbol);
}
auto ParserContext::Handle_Lvalue_Global_Identifier(const std::string& id, const Location& loc) -> ExpressionSPtr {
    const Symbol* existingSymbol = symbolTable->LookUpAtScope(id, SymbolTable::GlobalScope);
    if (existingSymbol == nullptr) {
        std::stringstream ss{};
        ss << "symbol undefined in global scope";
        SemanticError(loc, id, ss.str());
        existingSymbol = hiddenSymbolGenerator->GetTemporaryVariable(loc);
    }
    return Expression::Make(ExpressionTypeFromSymbolType(existingSymbol->type), existingSymbol);
}
auto ParserContext::Handle_Lvalue_TableItem(const ExpressionSPtr& tableItem, const Location& loc) -> ExpressionSPtr {
    (void)loc;
    return tableItem;
}

auto ParserContext::Handle_TableItem_Lvalue_Identifier(const ExpressionSPtr& lvalue, const std::string& id, const Location& loc) -> ExpressionSPtr {
    return ParserContext::Handle_TableItem_Lvalue_Expression(lvalue, Expression::MakeConstString(id), loc);
}
auto ParserContext::Handle_TableItem_Lvalue_Expression(const ExpressionSPtr& lvalue, const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    assert(lvalue->GetSymbol());
    if (lvalue->GetType() != ExpressionType::TableItem) return Expression::MakeTableItem(lvalue->GetSymbol(), expression);
    ExpressionSPtr lvalueNew = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::TableGetElement, lvalueNew, lvalue, lvalue->GetIndex(), loc);
    return Expression::MakeTableItem(lvalueNew->GetSymbol(), expression);
}
auto ParserContext::Handle_TableItem_Call_Identifier(const ExpressionSPtr& call, const std::string& id, const Location& loc) -> ExpressionSPtr {
    return ParserContext::Handle_TableItem_Lvalue_Identifier(call, id, loc);
}
auto ParserContext::Handle_TableItem_Call_Expression(const ExpressionSPtr& call, const ExpressionSPtr& expression, const Location& loc) -> ExpressionSPtr {
    return ParserContext::Handle_TableItem_Lvalue_Expression(call, expression, loc);
}

auto ParserContext::Handle_Call_NextCall(const ExpressionSPtr& call, const ExpressionListSPtr& expressionList, const Location& loc) -> ExpressionSPtr {
    for (const auto& expression : *expressionList | std::views::reverse)
        quadHolder->Emit(OpCode::Param, expression, nullptr, nullptr, loc);
    quadHolder->Emit(OpCode::Call, call, nullptr, nullptr, loc);
    std::vector<const Symbol*> recyclables{};
    std::ranges::transform(*expressionList, std::back_inserter(recyclables), [](const auto& e) { return e->GetSymbol(); });
    recyclables.emplace_back(call->GetSymbol());
    ExpressionSPtr returnValue = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->RecycleOrGetTemporaryVariable(recyclables, loc));
    quadHolder->Emit(OpCode::GetReturnValue, returnValue, nullptr, nullptr, loc);
    return returnValue;
}
auto ParserContext::Handle_Call_Lvalue_CallSuffix(const ExpressionSPtr& lvalue, const CallInfoSPtr& callSuffix, const Location& loc) -> ExpressionSPtr {
    ExpressionSPtr lvalueLocal = lvalue;
    if (lvalue->GetType() == ExpressionType::TableItem) {
        lvalueLocal = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
        quadHolder->Emit(OpCode::TableGetElement, lvalueLocal, lvalue, lvalue->GetIndex(), loc);
    }
    if (!callSuffix->isMethodCall)
        return ParserContext::Handle_Call_NextCall(lvalueLocal, callSuffix->expressionList, loc);
    assert(callSuffix->isMethodCall);
    callSuffix->expressionList->emplace(callSuffix->expressionList->begin(), lvalueLocal);
    ExpressionSPtr tableItem = Expression::MakeTableItem(lvalueLocal->GetSymbol(), Expression::MakeConstString(callSuffix->methodName));
    ExpressionSPtr lvalueMethod = Expression::Make(ExpressionType::Variable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::TableGetElement, lvalueMethod, tableItem, tableItem->GetIndex(), loc);
    return ParserContext::Handle_Call_NextCall(lvalueMethod, callSuffix->expressionList, loc);
}
auto ParserContext::Handle_Call_FunctionDefinition_ExpressionList(const Symbol* funcDef, const ExpressionListSPtr& expressionList, const Location& loc) -> ExpressionSPtr {
    assert(funcDef->type == SymbolType::UserFunction);
    return ParserContext::Handle_Call_NextCall(Expression::Make(ExpressionType::UserFunction, funcDef), expressionList, loc);
}
auto ParserContext::Handle_CallSuffix_NormalCall(const CallInfoSPtr& callInfo) -> CallInfoSPtr {
    return callInfo;
}
auto ParserContext::Handle_CallSuffix_MethodCall(const CallInfoSPtr& callInfo) -> CallInfoSPtr {
    return callInfo;
}
auto ParserContext::Handle_NormalCall(const ExpressionListSPtr& expressionList) -> CallInfoSPtr {
    return CallInfo::Make(expressionList, false, "");
}
auto ParserContext::Handle_MethodCall(const std::string& id, const ExpressionListSPtr& expressionList) -> CallInfoSPtr {
    return CallInfo::Make(expressionList, true, id);
}

auto ParserContext::Handle_ExpressionList_Empty() -> ExpressionListSPtr {
    return ExpressionList::Make();
}
auto ParserContext::Handle_ExpressionList_Expression(const ExpressionSPtr& expression) -> ExpressionListSPtr {
    return ParserContext::Handle_ExpressionList_NextExpression(ExpressionList::Make(), expression);
}
auto ParserContext::Handle_ExpressionList_NextExpression(const ExpressionListSPtr& expressionList, const ExpressionSPtr& expression) -> ExpressionListSPtr {
    expressionList->emplace_back(expression);
    return expressionList;
}

auto ParserContext::Handle_TableDefinition_ExpressionList(const ExpressionListSPtr& expressionList, const Location& loc) -> ExpressionSPtr {
    const PairListSPtr pairList = PairList::Make();
    double idx = -1.0;
    std::ranges::transform(*expressionList, std::back_inserter(*pairList), [&idx](const auto& e) { idx += 1; return Pair{Expression::MakeConstNumber(idx), e}; });
    return ParserContext::Handle_TableDefinition_PairList(pairList, loc);
}
auto ParserContext::Handle_TableDefinition_PairList(const PairListSPtr& pairList, const Location& loc) -> ExpressionSPtr {
    std::vector<const Symbol*> temps{};
    std::ranges::for_each(*pairList, [&temps](const auto& p) { temps.emplace_back(p.first->GetSymbol()); temps.emplace_back(p.second->GetSymbol()); });
    ExpressionSPtr table = Expression::Make(ExpressionType::NewTable, hiddenSymbolGenerator->GetTemporaryVariable(loc));
    quadHolder->Emit(OpCode::TableCreate, table, nullptr, nullptr, loc);
    for (const auto& [key, value] : *pairList)
        quadHolder->Emit(OpCode::TableSetElement, value, table, key, loc);
    return table;
}
auto ParserContext::Handle_PairList_Pair(const Pair& pair) -> PairListSPtr {
    return ParserContext::Handle_PairList_NextPair(PairList::Make(), pair);
}
auto ParserContext::Handle_PairList_NextPair(const PairListSPtr& pairList, const Pair& pair) -> PairListSPtr {
    pairList->emplace_back(pair);
    return pairList;
}
auto ParserContext::Handle_Pair(const ExpressionSPtr& key, const ExpressionSPtr& value) -> Pair {
    return Pair{ key, value };
}

auto ParserContext::Handle_FunctionName(const std::string& id) -> std::string {
    auto name = id;
    if (id.empty()) {
        name = hiddenSymbolGenerator->GetUserFunctionName();
    }
    return name;
}
auto ParserContext::Handle_FunctionPrefix(const std::string& name_, const Location& loc) -> Symbol* {
    std::string name = name_;
    if (symbolTable->IsLibraryFunction(name)) {
        std::stringstream ss{};
        ss << "shadows [Library Function] \"" << name << "\"";
        SemanticError(loc, name, ss.str());
        name = hiddenSymbolGenerator->GetUserFunctionName();
    }
    const Symbol* const existingSymbol = symbolTable->LookUpAtScope(name, scopeStack->GetScopeNum());
    if (existingSymbol != nullptr) {
        std::stringstream ss{};
        ss << "[User Function] redefinition of [" << existingSymbol->TypeToString() << "] \"" << existingSymbol->name << "\"";
        SemanticError(loc, name, ss.str());
        name = hiddenSymbolGenerator->GetUserFunctionName();
    }
    Symbol* functionSymbol = symbolTable->InsertNoConst({ SymbolType::UserFunction, name, scopeStack->GetScopeNum(), ScopeSpace{}, loc, true, FuncDefInfo{ .intermediateAddress = quadHolder->GetNextLabelPlus(0) } });
    quadHolder->Emit(OpCode::FunctionStart, Expression::Make(ExpressionType::UserFunction, functionSymbol), nullptr, nullptr, loc);
    HandleScopeBegin(name);
    HandleScopeSpaceBegin(ScopeSpaceName::FormalArguments);
    return functionSymbol;
}
auto ParserContext::Handle_FunctionArguments(const FormalArgList& formalArgList) -> FormalArgList {
    HandleScopeSpaceBegin(ScopeSpaceName::FunctionLocals);
    return formalArgList;
}

auto ParserContext::Handle_FunctionBody() -> void {}
auto ParserContext::Handle_FunctionDefinition(Symbol* functionSymbol, const FormalArgList& formalArgList) -> const Symbol* {
    const auto totalLocals = scopeSpaceStack->GetOffset();
    HandleScopeSpaceEnd();
    HandleScopeSpaceEnd();
    HandleScopeEnd();
    assert(functionSymbol != nullptr);
    functionSymbol->funcDefInfo.totalLocals = totalLocals;
    functionSymbol->funcDefInfo.formalArgList = formalArgList;
    quadHolder->Emit(OpCode::FunctionEnd, Expression::Make(ExpressionType::UserFunction, functionSymbol), nullptr, nullptr, functionSymbol->location);
    return functionSymbol;
}

auto ParserContext::Handle_Constant_Number(double number) -> ExpressionSPtr {
    return Expression::MakeConstNumber(number);
}
auto ParserContext::Handle_Constant_String(const std::string& string) -> ExpressionSPtr {
    return Expression::MakeConstString(string);
}
auto ParserContext::Handle_Constant_Nil() -> ExpressionSPtr {
    return Expression::MakeNil();
}
auto ParserContext::Handle_Constant_Boolean(bool boolean) -> ExpressionSPtr {
    return Expression::MakeConstBoolean(boolean);
}

auto ParserContext::Handle_IdentifierList_Empty() -> FormalArgList { return {}; }
auto ParserContext::Handle_IdentifierList_Identifier(const std::string& id, const Location& loc) -> FormalArgList {
    return ParserContext::Handle_IdentifierList_NextIdentifier(FormalArgList{}, id, loc);
}
auto ParserContext::Handle_IdentifierList_NextError(FormalArgList formalArgList) -> FormalArgList {
    return formalArgList;
}

auto ParserContext::Handle_IdentifierList_NextIdentifier(FormalArgList formalArgList, const std::string& id, const Location& loc) -> FormalArgList {
    assert(scopeStack->IsScopeFunction());
    const Symbol* const existingSymbol = symbolTable->LookUpAtScope(id, scopeStack->GetScopeNum());
    if (symbolTable->IsLibraryFunction(id)) {
        std::stringstream ss{};
        ss << "shadows [Library Function] \"" << id << "\"";
        SemanticError(loc, id, ss.str());
        return formalArgList;
    }
    if (existingSymbol != nullptr) {
        assert(existingSymbol->type == SymbolType::FormalArg || existingSymbol->type == SymbolType::UserFunction || existingSymbol->type == SymbolType::LibraryFunction);
        std::stringstream ss{};
        ss << "[Formal Argument] redefinition of [" << existingSymbol->TypeToString() << "] \"" << existingSymbol->name << "\"";
        SemanticError(loc, id, ss.str());
        return formalArgList;
    }
    const Symbol* const insertedFormalArg = symbolTable->Insert({ SymbolType::FormalArg, id, scopeStack->GetScopeNum(), scopeSpaceStack->GetScopeSpace(), loc });
    scopeSpaceStack->IncrementOffset();
    formalArgList.emplace_back(insertedFormalArg);
    return formalArgList;
}

auto ParserContext::Handle_If() -> void {
    HandleScopeBegin(ScopeStackType::IfScopeName);
}
auto ParserContext::Handle_IfExpression(const ExpressionSPtr& expression, const Location& loc) -> Label {
    quadHolder->EmitConditionalJump(OpCode::IfEqual, expression, Expression::MakeConstBoolean(true), quadHolder->GetNextLabelPlus(2), loc);
    const Label ifStmtBeginLabel = quadHolder->GetNextLabelPlus(0);
    quadHolder->EmitIncompleteJump(loc);
    return ifStmtBeginLabel;
}
auto ParserContext::Handle_Else(const Location& loc) -> Label {
    HandleScopeEnd();
    HandleScopeBegin(ScopeStackType::IfScopeName);
    const Label elseStmtBeginLabel = quadHolder->GetNextLabelPlus(0);
    quadHolder->EmitIncompleteJump(loc);
    return elseStmtBeginLabel;
}
auto ParserContext::Handle_IfBody(const StatementSPtr& statement) -> StatementSPtr {
    return statement;
}
auto ParserContext::Handle_IfStatement(Label ifStmtBeginLabel, const StatementSPtr& ifStmt) -> StatementSPtr {
    quadHolder->PatchJumps({ ifStmtBeginLabel }, quadHolder->GetNextLabelPlus(0));
    HandleScopeEnd();
    return ifStmt;
}
auto ParserContext::Handle_IfElseStatement(Label ifStmtBeginLabel, const StatementSPtr& ifStmt, Label elseStmtBeginLabel, const StatementSPtr& elseStmt) -> StatementSPtr {
    quadHolder->PatchJumps({ ifStmtBeginLabel }, elseStmtBeginLabel + 1);
    quadHolder->PatchJumps({ elseStmtBeginLabel }, quadHolder->GetNextLabelPlus(0));
    HandleScopeEnd();
    return Statement::Merge(ifStmt, elseStmt);
}

auto ParserContext::Handle_While() -> Label {
    HandleScopeBegin(ScopeStackType::LoopScopeName);
    return quadHolder->GetNextLabelPlus(0);
}
auto ParserContext::Handle_WhileExpression(const ExpressionSPtr& expression, const Location& loc) -> Label {
    quadHolder->EmitConditionalJump(OpCode::IfEqual, expression, Expression::MakeConstBoolean(true), quadHolder->GetNextLabelPlus(2), loc);
    const Label stmtBeginLabel = quadHolder->GetNextLabelPlus(0);
    quadHolder->EmitIncompleteJump(loc);
    return stmtBeginLabel;
}
auto ParserContext::Handle_WhileBody(const StatementSPtr& statement) -> StatementSPtr { return statement; }
auto ParserContext::Handle_WhileStatement(Label exprBeginLabel, Label stmtBeginLabel, const StatementSPtr& statement, const Location& loc) -> void {
    quadHolder->EmitJump(exprBeginLabel, loc);
    quadHolder->PatchJumps({ stmtBeginLabel }, quadHolder->GetNextLabelPlus(0));
    quadHolder->PatchJumps(statement->breakList, quadHolder->GetNextLabelPlus(0));
    quadHolder->PatchJumps(statement->continueList, exprBeginLabel);
    HandleScopeEnd();
}

auto ParserContext::Handle_Label() -> Label {
    return quadHolder->GetNextLabelPlus(0);
}
auto ParserContext::Handle_LabelJump(const Location& loc) -> Label {
    const Label jumpLabel = quadHolder->GetNextLabelPlus(0);
    quadHolder->EmitIncompleteJump(loc);
    return jumpLabel;
}
auto ParserContext::Handle_For() -> void {
    HandleScopeBegin(ScopeStackType::LoopScopeName);
}
auto ParserContext::Handle_ForExpression(const ExpressionSPtr& expression, const Location& loc) -> Label {
    const Label jumpLabel = quadHolder->GetNextLabelPlus(0);
    quadHolder->EmitIncompleteConditionalJump(OpCode::IfEqual, expression, Expression::MakeConstBoolean(true), loc);
    return jumpLabel;
}
auto ParserContext::Handle_ForBody(const StatementSPtr& statement) -> StatementSPtr { return statement; }
auto ParserContext::Handle_ForStatement(Label exprBeginLabel, Label exprEndLabel, Label eListBeginLabel, Label stmtBeginLabel, const StatementSPtr& statement, Label stmtEndLabel) -> void {
    quadHolder->PatchJumps({ exprEndLabel }, stmtBeginLabel + 1);
    quadHolder->PatchJumps({ eListBeginLabel }, quadHolder->GetNextLabelPlus(0));
    quadHolder->PatchJumps({ stmtBeginLabel }, exprBeginLabel);
    quadHolder->PatchJumps({ stmtEndLabel }, eListBeginLabel + 1);

    quadHolder->PatchJumps(statement->breakList, quadHolder->GetNextLabelPlus(0));
    quadHolder->PatchJumps(statement->continueList, eListBeginLabel + 1);
    HandleScopeEnd();
}

auto ParserContext::Handle_ReturnStatement_Empty(const Location& loc) -> void {
    if (!scopeStack->IsScopeFunctionEnclosed()) SemanticError(loc, {}, GetReturnError());
    quadHolder->Emit(OpCode::Return, {}, {}, {}, loc);
}
auto ParserContext::Handle_ReturnStatement_Expression(const ExpressionSPtr& expression, const Location& loc) -> void {
    if (!scopeStack->IsScopeFunctionEnclosed()) SemanticError(loc, {}, GetReturnError());
    quadHolder->Emit(OpCode::Return, expression, {}, {}, loc);
}
