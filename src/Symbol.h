#ifndef SYMBOL_H
#define SYMBOL_H

#include "Location.h"
#include "ScopeSpace.h"

#include <iosfwd>
#include <string>
#include <vector>

enum class SymbolType {
    TempVar,
    GlobalVar,
    LocalVar,
    FormalArg,
    UserFunction,
    LibraryFunction
};

struct Symbol;
using Label = size_t;
struct FuncDefInfo {
    using FormalArgList = std::vector<const Symbol*>;
    using Offset = unsigned long;
    Offset totalLocals{};
    FormalArgList formalArgList{};
    Label intermediateAddress{};
    Label targetAddress{};
};
using FormalArgList = FuncDefInfo::FormalArgList;
auto operator<<(std::ostream& os, const FormalArgList& obj) -> std::ostream&;

struct Symbol {
    using Scope = unsigned long;
    SymbolType type{};
    std::string name{};
    Scope scope{};
    ScopeSpace scopeSpace{};
    Location location{};
    bool isActive{ true };
    FuncDefInfo funcDefInfo{};
    auto TypeToString() const -> std::string;
};
auto operator<<(std::ostream& os, const Symbol& symbol) -> std::ostream&;

#endif
