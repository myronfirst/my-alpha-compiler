#ifndef QUAD_HOLDER_H
#define QUAD_HOLDER_H

#include "Location.h"

#include "JumpLabelList.h"

#include <memory>
#include <vector>

struct Expression;
using ExpressionSPtr = std::shared_ptr<Expression>;
using Label = size_t;

enum class OpCode {
    Assign,
    Add,
    Subtract,
    Multiply,
    Divide,
    Modulo,
    UnaryMinus,    // unused
    And,
    Or,
    Not,
    IfEqual,
    IfNotEqual,
    IfLess,
    IfLessEqual,
    IfGreater,
    IfGreaterEqual,
    Jump,
    Call,
    Param,
    Return,
    GetReturnValue,
    FunctionStart,
    FunctionEnd,
    TableCreate,
    TableGetElement,
    TableSetElement
};

struct Quad {
    OpCode opcode{};
    ExpressionSPtr result{};
    ExpressionSPtr arg1{};
    ExpressionSPtr arg2{};
    Label label{};
    Location location{};
};
auto operator<<(std::ostream& os, const Quad& quad) -> std::ostream&;

class QuadHolder {
public:
    auto GetNextLabelPlus(size_t step) const -> Label;
    auto Emit(OpCode opcode, ExpressionSPtr result, ExpressionSPtr arg1, ExpressionSPtr arg2, Location loc) -> void;
    auto EmitJump(Label label, Location loc) -> void;
    auto EmitIncompleteJump(Location loc) -> void;
    auto EmitConditionalJump(OpCode opCode, ExpressionSPtr arg1, ExpressionSPtr arg2, Label label, Location loc) -> void;
    auto EmitIncompleteConditionalJump(OpCode opcode, ExpressionSPtr arg1, ExpressionSPtr arg2, Location loc) -> void;
    auto PatchJumps(JumpLabelList indexList, Label label) -> void;
    auto AssertValid() const -> void;
    auto PrintTo(std::ostream& os) const -> void;

    auto Clear() -> void { quads.clear(); }

private:
    std::vector<Quad> quads{};
};

#endif
