#include "Compiler.h"

#include "Context.h"
#include "Parser.h"
#include "ParserContext.h"
#include "ParserUtilities.h"
#include "QuadHolder.h"
#include "Scanner.h"
#include "ScannerContext.h"
#include "ScannerUtilities.h"
#include "Symbol.h"
#include "SymbolTable.h"

#include <chrono>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include <cassert>

namespace {
    constexpr std::string_view FlexDebugPath = "flex.txt";
    constexpr std::string_view BisonDebugPath = "bison.txt";
}    // namespace

Compiler::Compiler(std::string_view inputPath_,
                   bool shortCircuitEvaluationEnabled_,
                   std::string_view quadsPath_,
                   std::string_view symbolTablePath_,
                   std::string_view tokensPath_)
    : inputPath{ std::move(inputPath_) },
      quadsPath{ std::move(quadsPath_) },
      symbolTablePath{ std::move(symbolTablePath_) },
      tokensPath{ std::move(tokensPath_) },
      context{ std::make_unique<Context>(shortCircuitEvaluationEnabled_) } {
    std::ifstream inputfs{ inputPath.data() };
    if (!inputfs.is_open()) {
        std::cout << "Failed to open " << inputPath << '\n';
        std::exit(EXIT_FAILURE);
    }
    for (const auto& path : { quadsPath, symbolTablePath, tokensPath, FlexDebugPath, BisonDebugPath }) {
        std::ofstream ofs{ path.data() };
        if (!ofs.is_open()) {
            std::cout << "Failed to open " << path << '\n';
            std::exit(EXIT_FAILURE);
        }
    }
}

Compiler::~Compiler() {
    yylex_destroy();    // Fix for Flex memory leak
}

auto Compiler::Compile() const -> void {
    std::ofstream flexfs{ FlexDebugPath.data() };
    std::ofstream bisonfs{ BisonDebugPath.data() };
    std::unique_ptr<FILE, decltype(&fclose)> inputfs(fopen(inputPath.data(), "r"), &fclose);
    {
        yyset_debug(true);
        freopen(FlexDebugPath.data(), "w", stderr);
        yyset_in(inputfs.get());
        yy::parser parser{ context.get() };
        parser.set_debug_level(true);
        parser.set_debug_stream(bisonfs);

        const auto timePointBegin = std::chrono::high_resolution_clock::now();
        bool fail = parser.parse();
        const auto timePointEnd = std::chrono::high_resolution_clock::now();
        const auto compileTime = std::chrono::duration_cast<std::chrono::milliseconds>(timePointEnd - timePointBegin);
        // std::cout << "Execution Time in Milliseconds (excl. result prints): " << compileTime.count() << '\n';
        if (fail) {
            std::cout << "Intermediate Code Generation aborted due to parsing errors\n";
            return;
        }
        context->ParserCtx()->GetSymbolTable()->AssertValid();
        context->ParserCtx()->GetQuadHolder()->AssertValid();
    }

    {
        // Phase 1 output
        std::ofstream fs{ tokensPath.data() };
        context->ScannerCtx()->GetTokenHolder()->PrintTo(fs);
    }
    {
        // Phase 2 output
        std::ofstream fs{ symbolTablePath.data() };
        context->ParserCtx()->GetSymbolTable()->PrintTo(fs);
    }
    {
        // Phase 3 output
        std::ofstream fs{ quadsPath.data() };
        context->ParserCtx()->GetQuadHolder()->PrintTo(fs);
    }
}
