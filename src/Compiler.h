#ifndef ALPHA_COMPILER_H
#define ALPHA_COMPILER_H

#include <memory>
#include <string_view>

struct Context;
class Compiler {
public:
    explicit Compiler(std::string_view, bool = true, std::string_view = "Quads.quads", std::string_view = "SymbolTable.txt", std::string_view = "Tokens.txt");
    ~Compiler();
    auto Compile() const -> void;

private:
    std::string_view inputPath;
    std::string_view quadsPath;
    std::string_view symbolTablePath;
    std::string_view tokensPath;

    std::unique_ptr<Context> context;
};

#endif
