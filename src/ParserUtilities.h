#ifndef PARSER_UTILITIES_H
#define PARSER_UTILITIES_H

#include "Location.h"
#include "ScopeSpace.h"

#include <string>
#include <vector>

class Symbol;
class SymbolTable;
class ParserContext;

/*
    Class responsible for handling scope rules of the alpha language.
    Holds a stack of the active scopes. Each element is the name of the scope.
    A function scope is named after the function itself.
    A block, if, while, for scope and the global scope have reserved names.
    The class is typically used on Scope Begin and End handlers.
*/
class ScopeStackType {
public:
    using Scope = unsigned long;
    constexpr static auto GlobalScopeName = "#Global";
    constexpr static auto BlockScopeName = "#Block";
    constexpr static auto IfScopeName = "#If";
    constexpr static auto LoopScopeName = "#Loop";
    auto Push(const std::string& name) -> void;
    auto Pop() -> void;
    auto GetSize() const -> size_t;
    static auto IsFunctionName(const std::string& name) -> bool;
    static auto IsLoopName(const std::string& name) -> bool;
    auto GetScopeName() const -> std::string;
    auto GetScopeNum() const -> size_t;
    auto IsScopeGlobal() const -> bool;
    auto IsScopeBlock() const -> bool;
    auto IsScopeIf() const -> bool;
    auto IsScopeLoop() const -> bool;
    auto IsScopeLoopEnclosed() const -> bool;
    auto IsScopeFunction() const -> bool;
    auto IsScopeFunctionEnclosed() const -> bool;
    auto IsSymbolOutsideEnclosingFunction(Scope scope) const -> bool;

private:
    std::vector<std::string> stack{ GlobalScopeName };
};

class ScopeSpaceStackType {
public:
    using Offset = unsigned long;
    auto Push(ScopeSpace scopeSpace) -> void;
    auto Pop() -> void;
    auto GetSize() const -> size_t;
    auto GetScopeSpace() const -> ScopeSpace;
    auto GetOffset() const -> Offset;
    auto IncrementOffset(Offset val = 1UL) -> void;

private:
    std::vector<ScopeSpace> stack{ { ScopeSpaceName::ProgramVariables, 0UL } };
};

class HiddenSymbolGeneratorType {
public:
    HiddenSymbolGeneratorType(SymbolTable* symbolTable_, ScopeStackType* scopeStack_, ScopeSpaceStackType* scopeSpaceStack_);
    auto GetUserFunctionName() -> std::string;
    auto ResetTemporaryVariableNames() -> void;
    auto GetTemporaryVariable(const Location& loc) -> const Symbol*;
    auto RecycleOrGetTemporaryVariable(std::vector<const Symbol*> symbolList, const Location& loc) -> const Symbol*;

private:
    auto GetTemporaryVariableName() -> std::string;
    auto IsTemporaryVariableName(const std::string& name) -> bool;
    size_t userFunctionCounter{ 0UL };
    size_t temporaryVariableCounter{ 0UL };
    constexpr static auto UserFunctionPrefix = "$f";
    constexpr static auto TemporaryVariablePrefix = "_t";
    SymbolTable* symbolTable;
    ScopeStackType* scopeStack;
    ScopeSpaceStackType* scopeSpaceStack;
};

#endif
