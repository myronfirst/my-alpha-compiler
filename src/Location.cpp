#include "Location.h"

#include <ostream>

namespace {
    using Counter = Location::Counter;
    Counter add_(Counter lhs, Counter rhs, Counter min) {
        return lhs + rhs < min ? min : lhs + rhs;
    }
}    // namespace

auto Position::Lines(Counter count) -> void {
    if (count) {
        column = 1;
        line = add_(line, count, 1);
    }
}
auto Position::Columns(Counter count) -> void {
    column = add_(column, count, 1);
}
auto operator+=(Position& res, Counter width) -> Position& {
    res.Columns(width);
    return res;
}
auto operator<<(std::ostream& os, const Position& pos) -> std::ostream& {
    return os << pos.line << '.' << pos.column;
}

auto Location::Lines(Counter count) -> void {
    end.Lines(count);
}
auto Location::Columns(Counter count) -> void {
    end += count;
}
auto Location::Step() -> void {
    begin = end;
}
auto operator<<(std::ostream& os, const Location& loc) -> std::ostream& {
    Counter end_col = 0 < loc.end.column ? loc.end.column - 1 : 0;
    os << loc.begin;
    if (loc.begin.line < loc.end.line)
        os << '-' << loc.end.line << '.' << end_col;
    else if (loc.begin.column < end_col)
        os << '-' << end_col;
    return os;
}
