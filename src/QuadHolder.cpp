#include "QuadHolder.h"

#include "Expression.h"
#include "JumpLabelList.h"
#include "Location.h"

#include <iomanip>
#include <limits>
#include <sstream>

#include <cassert>

namespace {
    auto OpCodeToString(OpCode opcode) -> std::string {
        switch (opcode) {
            case OpCode::Assign: return "Assign";
            case OpCode::Add: return "Add";
            case OpCode::Subtract: return "Subtract";
            case OpCode::Multiply: return "Multiply";
            case OpCode::Divide: return "Divide";
            case OpCode::Modulo: return "Modulo";
            case OpCode::UnaryMinus: return "UnaryMinus";
            case OpCode::And: return "And";
            case OpCode::Or: return "Or";
            case OpCode::Not: return "Not";
            case OpCode::IfEqual: return "IfEqual";
            case OpCode::IfNotEqual: return "IfNotEqual";
            case OpCode::IfLess: return "IfLess";
            case OpCode::IfLessEqual: return "IfLessEqual";
            case OpCode::IfGreater: return "IfGreater";
            case OpCode::IfGreaterEqual: return "IfGreaterEqual";
            case OpCode::Jump: return "Jump";
            case OpCode::Call: return "Call";
            case OpCode::Param: return "Param";
            case OpCode::Return: return "Return";
            case OpCode::GetReturnValue: return "GetReturnValue";
            case OpCode::FunctionStart: return "FunctionStart";
            case OpCode::FunctionEnd: return "FunctionEnd";
            case OpCode::TableCreate: return "TableCreate";
            case OpCode::TableGetElement: return "TableGetElement";
            case OpCode::TableSetElement: return "TableSetElement";
            default: assert(false); return {};
        }
    }
    auto IsJump(OpCode opcode) -> bool {
        switch (opcode) {
            case OpCode::IfEqual:
            case OpCode::IfNotEqual:
            case OpCode::IfLess:
            case OpCode::IfLessEqual:
            case OpCode::IfGreater:
            case OpCode::IfGreaterEqual:
            case OpCode::Jump: return true;
            default: return false;
        }
    }
}    // namespace

auto QuadHolder::GetNextLabelPlus(size_t step) const -> Label { return quads.size() + step; }
auto QuadHolder::Emit(OpCode opcode, ExpressionSPtr result, ExpressionSPtr arg1, ExpressionSPtr arg2, Location loc) -> void {
    quads.emplace_back(opcode, result, arg1, arg2, 0U, loc);
}
auto QuadHolder::EmitJump(Label label, Location loc) -> void {
    quads.emplace_back(OpCode::Jump, nullptr, nullptr, nullptr, label, loc);
}
auto QuadHolder::EmitIncompleteJump(Location loc) -> void {
    EmitJump(std::numeric_limits<Label>::max(), loc);
}
auto QuadHolder::EmitConditionalJump(OpCode opcode, ExpressionSPtr arg1, ExpressionSPtr arg2, Label label, Location loc) -> void {
    assert(opcode == OpCode::IfEqual || opcode == OpCode::IfNotEqual || opcode == OpCode::IfLess || opcode == OpCode::IfLessEqual || opcode == OpCode::IfGreater || opcode == OpCode::IfGreaterEqual);
    quads.emplace_back(opcode, nullptr, arg1, arg2, label, loc);
}
auto QuadHolder::EmitIncompleteConditionalJump(OpCode opcode, ExpressionSPtr arg1, ExpressionSPtr arg2, Location loc) -> void {
    EmitConditionalJump(opcode, arg1, arg2, std::numeric_limits<Label>::max(), loc);
}
auto QuadHolder::PatchJumps(JumpLabelList indexList, Label label) -> void {
    for (const auto& index : indexList) {
        auto& jumpQuad = quads.at(index);
        assert(jumpQuad.label == std::numeric_limits<Label>::max() &&
               (jumpQuad.opcode == OpCode::Jump || jumpQuad.opcode == OpCode::IfEqual || jumpQuad.opcode == OpCode::IfNotEqual || jumpQuad.opcode == OpCode::IfLess || jumpQuad.opcode == OpCode::IfLessEqual || jumpQuad.opcode == OpCode::IfGreater || jumpQuad.opcode == OpCode::IfGreaterEqual));
        jumpQuad.label = label;
    }
}

namespace {
#ifdef RELEASE_PRINTS
    constexpr auto ExprWidth = 15;
#else
    constexpr auto ExprWidth = 35;
#endif

    constexpr auto IndexWidth = 10;
    constexpr auto OpCodeWidth = 20;
    constexpr auto ResultWidth = ExprWidth;
    constexpr auto Arg1Width = ExprWidth;
    constexpr auto Arg2Width = ExprWidth;
    constexpr auto LabelWidth = 10;
    constexpr auto LocationWidth = 10;
    constexpr auto CellFill = ' ';
    constexpr auto TotalWidth = IndexWidth + OpCodeWidth + ResultWidth + Arg1Width + Arg2Width + LabelWidth + LocationWidth;
}    // namespace

auto QuadHolder::AssertValid() const -> void {
    const auto assertQuadFields = [](const Quad& q, size_t quadsSize) {
        switch (q.opcode) {
            // Valid fields: result, arg1, arg2
            case OpCode::Add:
            case OpCode::Subtract:
            case OpCode::Multiply:
            case OpCode::Divide:
            case OpCode::Modulo:
            case OpCode::And:
            case OpCode::Or:
            case OpCode::TableGetElement:
            case OpCode::TableSetElement:
                assert(q.result);
                assert(q.arg1);
                assert(q.arg2);
                assert(q.label == 0);
                break;
            // Valid fields: result, arg1
            case OpCode::Assign:
            case OpCode::UnaryMinus:
            case OpCode::Not:
                assert(q.result);
                assert(q.arg1);
                assert(!q.arg2);
                assert(q.label == 0);
                break;
            // Valid fields: arg1, arg2, label
            case OpCode::IfEqual:
            case OpCode::IfNotEqual:
            case OpCode::IfLess:
            case OpCode::IfLessEqual:
            case OpCode::IfGreater:
            case OpCode::IfGreaterEqual:
                assert(!q.result);
                assert(q.arg1);
                assert(q.arg2);
                assert(q.label <= quadsSize);    // Check for Incomplete Jump
                break;
            // Valid fields: result
            case OpCode::Call:
            case OpCode::Param:
            case OpCode::GetReturnValue:
            case OpCode::TableCreate:
            case OpCode::FunctionStart:
            case OpCode::FunctionEnd:
                assert(q.result);
                assert(!q.arg1);
                assert(!q.arg2);
                assert(q.label == 0);
                break;
            // Valid fields: label
            case OpCode::Jump:
                assert(!q.result);
                assert(!q.arg1);
                assert(!q.arg2);
                assert(q.label <= quadsSize);    // Check for Incomplete Jump
                break;
            // Opcode::Return valid fields are either result or None
            case OpCode::Return:
                assert(!q.arg1);
                assert(!q.arg2);
                assert(q.label == 0);
                break;
            default: assert(false); break;
        }
    };
    const auto assertExpressionData = [](const Expression* e) {
        if (e == nullptr) return;
        switch (e->type) {
            case ExpressionType::TableItem:
                assert(e->symbol);
                assert(std::holds_alternative<Expression::Index>(e->data));
                break;
            case ExpressionType::ConstNumber:
                assert(e->symbol == nullptr);
                assert(std::holds_alternative<Expression::ConstNumber>(e->data));
                break;
            case ExpressionType::ConstBoolean:
                assert(e->symbol == nullptr);
                assert(std::holds_alternative<Expression::ConstBoolean>(e->data));
                break;
            case ExpressionType::ConstString:
                assert(e->symbol == nullptr);
                assert(std::holds_alternative<Expression::ConstString>(e->data));
                break;
            case ExpressionType::Nil:
                assert(e->symbol == nullptr);
                break;
            default: break;
        }
    };
    const auto sz = quads.size();
    for (const auto& quad : quads) {
        assertQuadFields(quad, sz);
        assertExpressionData(quad.result.get());
        assertExpressionData(quad.arg1.get());
        assertExpressionData(quad.arg2.get());
    }
}

auto QuadHolder::PrintTo(std::ostream& os) const -> void {
    size_t idx = 0;
    for (const auto& quad : quads) {
        os << std::left << std::setw(IndexWidth) << std::setfill(CellFill) << idx
           << quad
           << '\n';
        idx += 1;
    }
}

auto operator<<(std::ostream& os, const Quad& quad) -> std::ostream& {
    std::stringstream ss{};
    ss << OpCodeToString(quad.opcode);
    const auto opcode = ss.str();
    ss.str("");
    (quad.result) ? (ss << *(quad.result)) : (ss << "");
    const auto result = ss.str();
    ss.str("");
    (quad.arg1) ? (ss << *(quad.arg1)) : (ss << "");
    const auto arg1 = ss.str();
    ss.str("");
    (quad.arg2) ? (ss << *(quad.arg2)) : (ss << "");
    const auto arg2 = ss.str();
    ss.str("");
    IsJump(quad.opcode) ? (ss << quad.label) : (ss << "");
    const auto label = ss.str();
    ss.str("");
    ss << quad.location;
    const auto location = ss.str();
    ss.str("");
    os << std::left << std::setw(OpCodeWidth) << std::setfill(CellFill) << opcode
       << std::left << std::setw(ResultWidth) << std::setfill(CellFill) << result
       << std::left << std::setw(Arg1Width) << std::setfill(CellFill) << arg1
       << std::left << std::setw(Arg2Width) << std::setfill(CellFill) << arg2
       << std::left << std::setw(LabelWidth) << std::setfill(CellFill) << label
       << std::left << std::setw(LocationWidth) << std::setfill(CellFill) << location;
    return os;
}
