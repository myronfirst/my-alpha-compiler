#ifndef CONTEXT_H
#define CONTEXT_H

#include <memory>

class ScannerContext;
class ParserContext;
struct Context {
    explicit Context(bool);
    auto ScannerCtx() -> ScannerContext*;
    auto ParserCtx() -> ParserContext*;

    std::unique_ptr<ScannerContext> scannerContext;
    std::unique_ptr<ParserContext> parserContext;
};

#endif
