#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include "Location.h"
// #include "Symbol.h"

#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

struct Symbol;
class SymbolTableImplementation {
public:
    using Scope = unsigned long;
    virtual auto Insert(const Symbol& symbol) -> const Symbol* = 0;
    virtual auto LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol* = 0;
    virtual auto LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol* = 0;
    virtual auto Hide(Scope scope) -> void = 0;
    virtual auto IsLibraryFunction(const std::string& name) const -> bool = 0;
    virtual auto CheckScopeUpperLimit(Scope scope) -> void = 0;
    virtual auto AssertValid() const -> void = 0;
    virtual auto PrintTo(std::ostream& os) const -> void = 0;
    virtual ~SymbolTableImplementation() = default;
};
class SymbolTableStack final : public SymbolTableImplementation {
protected:
    using SymbolUPtr = std::unique_ptr<Symbol>;
    using SymbolMap = std::unordered_map<std::string, SymbolUPtr>;
    using SymbolMapHistory = std::vector<SymbolMap>;
    using Stack = std::vector<SymbolMapHistory>;
    auto Insert(const Symbol& symbol) -> const Symbol* override;
    auto LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol* override;
    auto LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol* override;
    auto Hide(Scope scope) -> void override;
    auto IsLibraryFunction(const std::string& name) const -> bool override;
    auto CheckScopeUpperLimit(Scope scope) -> void override;
    auto AssertValid() const -> void override;
    auto PrintTo(std::ostream& os) const -> void override;

private:
    Stack stack{};
    std::vector<std::vector<const Symbol*>> printScopeLists{};
};
class SymbolTableScopeLists final : public SymbolTableImplementation {
protected:
    using SymbolUPtr = std::unique_ptr<Symbol>;
    auto Insert(const Symbol& symbol) -> const Symbol* override;
    auto LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol* override;
    auto LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol* override;
    auto Hide(Scope scope) -> void override;
    auto IsLibraryFunction(const std::string& name) const -> bool override;
    auto CheckScopeUpperLimit(Scope scope) -> void override;
    auto AssertValid() const -> void override;
    auto PrintTo(std::ostream& os) const -> void override;

private:
    std::unordered_set<SymbolUPtr> table{};
    std::vector<std::vector<Symbol*>> scopeLists{};
};
class SymbolTable {
public:
    using Scope = unsigned long;
    using SymbolUPtr = std::unique_ptr<Symbol>;
    using ImplementationUPtr = std::unique_ptr<SymbolTableImplementation>;

    static constexpr Scope GlobalScope = 0U;
    SymbolTable();
    auto Clear() -> void;

    auto Insert(const Symbol& symbol) -> const Symbol*;
    auto InsertNoConst(const Symbol& symbol) -> Symbol*;
    auto LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol*;
    auto LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol*;
    auto LookUpAtScopeNoConst(const std::string& name, Scope scope) const -> Symbol*;
    auto Hide(Scope scope) -> void;
    auto IsLibraryFunction(const std::string& name) const -> bool;
    auto CheckScopeUpperLimit(Scope scope) -> void;
    auto AssertValid() const -> void;
    auto PrintTo(std::ostream& os) const -> void;

private:
    auto InstallLibraryFunctions() -> void;
    ImplementationUPtr impl{};
};

#endif
