#include "SymbolTable.h"

#include "Location.h"
#include "ScopeSpace.h"
#include "Symbol.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <span>
#include <sstream>

#include <cassert>

/* File Scope Symbols */

namespace {
    const auto LibraryFunctionNamesCollection = {
        "print",
        "input",
        "objectmemberkeys",
        "objecttotalmembers",
        "objectcopy",
        "totalarguments",
        "argument",
        "typeof",
        "strtonum",
        "sqrt",
        "cos",
        "sin"
    };

    auto PrintScopeLists(const auto& scopeLists, std::ostream& os) -> void {
        for (const auto& list : scopeLists) {
            for (const auto& symbol : list)
                os << *symbol << '\n';
        }
    }
}    // namespace
/* Interface Implementation */

/* New Implementation */

auto SymbolTableStack::Insert(const Symbol& symbol) -> const Symbol* {
    const auto scope = symbol.scope;
    const auto name = symbol.name;
    const auto [it, ok] = stack.at(scope).back().emplace(name, std::make_unique<Symbol>(symbol));
    assert(ok);
    const Symbol* const insertedSymbol = it->second.get();
    printScopeLists.at(scope).emplace_back(insertedSymbol);
    return insertedSymbol;
}
auto SymbolTableStack::LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol* {
    for (Symbol::Scope scopeDiff = 0; scopeDiff <= scope; ++scopeDiff) {
        const Symbol* const symbol = LookUpAtScope(name, scope - scopeDiff);
        if (symbol != nullptr) return symbol;
    }
    return nullptr;
}
auto SymbolTableStack::LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol* {
    const auto& map = stack.at(scope).back();
    const auto it = map.find(name);
    if (it == std::ranges::cend(map)) return nullptr;
    const Symbol* const symbol = it->second.get();
    assert(symbol->isActive);
    return symbol;
}

auto SymbolTableStack::Hide(Scope scope) -> void {
    auto DeactivateSymbols = [](auto& _stack, const auto& _scope) {
        for (auto& [_, s] : _stack.at(_scope).back())
            s->isActive = false;
        return true;
    };
    assert(DeactivateSymbols(stack, scope));
    stack.at(scope).emplace_back();
}

auto SymbolTableStack::IsLibraryFunction(const std::string& name) const -> bool {
    // optimization
    auto EqualTo = [](const auto& p) { return [&p](const auto& n) { return n == p; }; };
    return std::ranges::any_of(LibraryFunctionNamesCollection, EqualTo(name));

    // expected implementation
    auto EqualToTypeAndName = [](const auto& _type, const auto& _name) { return [&_type, &_name](const auto& it) { return it.second->type == _type && it.second->name == _name; }; };
    return std::ranges::any_of(stack.at(SymbolTable::GlobalScope).back(), EqualToTypeAndName(SymbolType::LibraryFunction, name));
}
auto SymbolTableStack::CheckScopeUpperLimit(Scope scope) -> void {
    assert(scope <= std::ranges::size(stack));
    stack.resize(std::max(std::ranges::size(stack), scope + 1LU));
    auto& history = stack.back();
    history.resize(std::max(std::ranges::size(history), 1LU));
    printScopeLists.resize(std::max(std::ranges::size(printScopeLists), scope + 1LU));
    assert(std::size(stack) == std::size(printScopeLists));
}
auto SymbolTableStack::AssertValid() const -> void {
    assert(stack.at(SymbolTable::GlobalScope).size() == 1);
    for (Scope scope = SymbolTable::GlobalScope; scope < stack.size(); ++scope) {
        const auto& history = stack.at(scope);
        for (const auto& map : history) {
            for (const auto& [key, symbol] : map) {
                assert(key == symbol->name);
                assert(symbol->scope == scope);
                if (scope == SymbolTable::GlobalScope)
                    assert(symbol->isActive);
                else
                    assert(!symbol->isActive);
                if (symbol->type == SymbolType::UserFunction)
                    for (const auto formal : symbol->funcDefInfo.formalArgList)
                        assert(formal->type == SymbolType::FormalArg);
            }
        }
    }
}

auto SymbolTableStack::PrintTo(std::ostream& os) const -> void {
    PrintScopeLists(printScopeLists, os);
}

/* Old Implementation */

auto SymbolTableScopeLists::Insert(const Symbol& symbol) -> const Symbol* {
    const auto [it, ok] = table.emplace(std::make_unique<Symbol>(symbol));
    assert(ok);
    Symbol* const symbolObserver = it->get();
    const auto scope = symbolObserver->scope;
    scopeLists.at(scope).emplace_back(symbolObserver);
    return symbolObserver;
}
auto SymbolTableScopeLists::LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol* {
    for (Symbol::Scope scopeDiff = 0; scopeDiff <= scope; ++scopeDiff) {
        const Symbol* const symbol = LookUpAtScope(name, scope - scopeDiff);
        if (symbol != nullptr) return symbol;
    }
    return nullptr;
}
auto SymbolTableScopeLists::LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol* {
    for (const auto* const s : scopeLists.at(scope)) {
        if (s->scope == SymbolTable::GlobalScope) assert(s->isActive);
        if (s->isActive && name == s->name) return s;
    }
    return nullptr;
}

auto SymbolTableScopeLists::Hide(Scope scope) -> void {
    for (auto* const s : scopeLists.at(scope))
        s->isActive = false;
}

auto SymbolTableScopeLists::IsLibraryFunction(const std::string& name) const -> bool {
    // optimization
    auto EqualToName = [](const auto& _name) { return [&_name](const auto& i) { return i == _name; }; };
    return std::ranges::any_of(LibraryFunctionNamesCollection, EqualToName(name));

    // expected implementation
    auto EqualToTypeAndName = [](const auto& _type, const auto& _name) { return [&_type, &_name](const auto& i) { return i->type == _type && i->name == _name; }; };
    return std::ranges::any_of(scopeLists.at(SymbolTable::GlobalScope), EqualToTypeAndName(SymbolType::LibraryFunction, name));
}
auto SymbolTableScopeLists::CheckScopeUpperLimit(Scope scope) -> void {
    assert(scope <= std::ranges::size(scopeLists));
    scopeLists.resize(std::max(std::ranges::size(scopeLists), scope + 1LU));
}
auto SymbolTableScopeLists::AssertValid() const -> void {
    // This function is not tested, since SymbolTableScopeLists implementation is unused
    for (Scope scope = SymbolTable::GlobalScope; scope < scopeLists.size(); ++scope) {
        const auto& list = scopeLists.at(scope);
        for (const auto& symbol : list) {
            assert(symbol->scope == scope);
            if (scope == SymbolTable::GlobalScope)
                assert(symbol->isActive);
            else
                assert(!symbol->isActive);
            if (symbol->type == SymbolType::UserFunction)
                for (const auto formal : symbol->funcDefInfo.formalArgList)
                    assert(formal->type == SymbolType::FormalArg);
        }
    }
}
auto SymbolTableScopeLists::PrintTo(std::ostream& os) const -> void {
    PrintScopeLists(scopeLists, os);
}

/* Front End Operations */

auto SymbolTable::Insert(const Symbol& symbol) -> const Symbol* {
    return impl->Insert(symbol);
}
auto SymbolTable::InsertNoConst(const Symbol& symbol) -> Symbol* { return const_cast<Symbol*>(Insert(symbol)); }

auto SymbolTable::LookUpAcrossScopes(const std::string& name, Scope scope) const -> const Symbol* {
    return impl->LookUpAcrossScopes(name, scope);
}

auto SymbolTable::LookUpAtScope(const std::string& name, Scope scope) const -> const Symbol* {
    return impl->LookUpAtScope(name, scope);
}
auto SymbolTable::LookUpAtScopeNoConst(const std::string& name, Scope scope) const -> Symbol* { return const_cast<Symbol*>(LookUpAtScope(name, scope)); }
auto SymbolTable::Hide(Scope scope) -> void {
    impl->Hide(scope);
}

auto SymbolTable::IsLibraryFunction(const std::string& name) const -> bool {
    return impl->IsLibraryFunction(name);
}
auto SymbolTable::CheckScopeUpperLimit(Scope scope) -> void {
    impl->CheckScopeUpperLimit(scope);
}
auto SymbolTable::AssertValid() const -> void {
    impl->AssertValid();
}

auto SymbolTable::PrintTo(std::ostream& os) const -> void {
    impl->PrintTo(os);
}

SymbolTable::SymbolTable() {
    // impl = std::make_unique<SymbolTableScopeLists>();    // Old implementation
    impl = std::make_unique<SymbolTableStack>();    // New implementation
    CheckScopeUpperLimit(SymbolTable::GlobalScope);
    InstallLibraryFunctions();
}
auto SymbolTable::Clear() -> void {
    // impl = std::make_unique<SymbolTableScopeLists>();    // Old implementation
    impl = std::make_unique<SymbolTableStack>();    // New implementation
    CheckScopeUpperLimit(SymbolTable::GlobalScope);
    InstallLibraryFunctions();
}

auto SymbolTable::InstallLibraryFunctions() -> void {
    for (const auto* const name : LibraryFunctionNamesCollection) {
        Insert({ SymbolType::LibraryFunction, name, SymbolTable::GlobalScope, ScopeSpace{}, Location{} });
    }
}
