#ifndef SCANNER_CONTEXT_H
#define SCANNER_CONTEXT_H

#include "Location.h"
#include "Parser.h"

#include <iosfwd>
#include <map>
#include <memory>
#include <stack>
#include <string>
#include <vector>

class TokenHolder;
class ScannerContext {
private:
    size_t tokenNum = 1;
    Location locationCursor{};
    std::string stringBuffer{};
    Location stringLocationBegin{};
    std::stack<Location> blockCommentLocationBeginStack{};
    bool noPendingBlockComments{};
    std::unique_ptr<TokenHolder> tokenHolder;

    using MakeTokenFunction = std::add_pointer_t<yy::parser::symbol_type(const Location&)>;
    static std::map<std::string, MakeTokenFunction> NonSemanticTokensCtorMap;
    static std::map<std::string, std::string> LexicalTokenTypeMap;

public:
    ScannerContext();
    auto GetTokenHolder() const -> TokenHolder*;

    auto LocationColumns(int length) -> void;
    auto LocationStep() -> void;
    auto NoPendingBlockComments() const -> bool;

    auto HandleSpaces(const char* text, int length) -> void;
    auto HandleNewlines(const char* text, int length) -> void;
    auto HandleUndefinedChar(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleKeyword(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleOperand(const char* text, int length) -> yy::parser::symbol_type;
    auto HandlePunctuation(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleInteger(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleReal(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleId(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleLineComment(const char* text, int length) -> void;
    auto HandleEOF(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleNoRuleMatch(const char* text, int length) -> yy::parser::symbol_type;

    auto HandleStringBegin(const char* text, int length) -> void;
    auto HandleStringEnd(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleStringEscapeChar(const char* text, int length) -> void;
    auto HandleStringEscapeInvalid(const char* text, int length) -> void;
    auto HandleStringUnterminated(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleStringDefault(const char* text, int length) -> void;
    auto HandleStringNewlines(const char* text, int length) -> void;

    auto HandleBlockCommentBegin(const char* text, int length) -> void;
    auto HandleBlockCommentEnd(const char* text, int length) -> void;
    auto HandleBlockCommentUnterminated(const char* text, int length) -> yy::parser::symbol_type;
    auto HandleBlockCommentNewlines(const char* text, int length) -> void;
    auto HandleBlockCommentDefault(const char* text, int length) -> void;
};

#endif
