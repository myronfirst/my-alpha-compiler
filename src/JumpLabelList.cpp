
#include "JumpLabelList.h"

auto JumpLabelList::operator+=(const JumpLabelList& rhs) -> JumpLabelList& {
    this->insert(std::end(*this), std::cbegin(rhs), std::cend(rhs));
    return *this;
}
auto operator+(JumpLabelList lhs, const JumpLabelList& rhs) -> JumpLabelList {
    lhs += rhs;
    return lhs;
}
